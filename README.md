# ERC1400 Smart Contract Implementation

[ERC1400](https://github.com/ethereum/EIPs/issues/1411) is a proposed standard
for _Security Tokens_ on Ethereum. This repository contains the implementation
used by the Talium Assets platform.

## Notes on the Implementation

![diagram](./docs/overview.png)

The main contract `ERC1400` delegates transaction validation to another
contract that ust implement the `ITransactionValidator` interface. The provided
implementation (`TransactionValidator`) checks that the `data` parameter is a
certificate signed by a trusted private key.

![diagram](./docs/transaction.png)

Some code was initially based on the
[reference implementation](https://github.com/SecurityTokenStandard/EIP-Spec)
but there are now significant changes.

### Implementation Details

- `controllerTransfer` and `controllerRedeem` are not included, the functions
  occupy space and controllers can use `operatorTransferByPartition` or
  `operatorRedeemByPartition` instead.
- No `ChangedPartition` event is emitted. It is not clear whether emitting
  this event is required (it appears in the ERC1400 but not in the ERC1410
  interface).
- When invoking `operatorTransferByPartition`, it is possible to specify the
  destination partition using the `operatorData` parameter.
- All the revert error messages are just
  [EIP-1066](https://eips.ethereum.org/EIPS/eip-1066) status codes to save
  space and avoid including long strings in the bytecode.
- There is a default partition used by operations which do not specify a
  partition explicitly. This default partition can be updated by the contract
  owner (`setDefaultPartition`), but the token holders have not control over
  it.

### ERC-20 Compatibility

It is possible to implement ERC-20 compatibility by providing an implementation
of the `ITransactionValidator` interface that used rules and/or white lists to
check if transactions are compliant.

### Upgradability

The contract does not directly handle upgradability, it is meant to be used
with a generic transparent proxy (tested with the OpenZeppelin proxy).
The contract implements the `Initializable` OpenZeppelin interface to that
effect (see [Proxy Upgrade Pattern](https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies)).

When used with a transparent proxy, the contract state is stored in the
proxy, not in the actual ERC1400 (it is only necessary to deploy one ERC1400
contract that will shared by all the proxies).

## Development Environment

### Requirements

- `node` version 12 (there is currently an issue with version 14, compilation
  of `sha3` module fails)
- `yarn` (tested with version 1.22.5)

### Unit Tests

The unit tests can be executed using `yarn test` (or using Truffle directly,
with `./node_modules/.bin/truffle test`).

Each test file can be executed independently using e.g.
`yarn test test/erc1594.js` or
`./node_modules/.bin/truffle test test/erc1594.js`

### Measuring Code Coverage

To measure unit tests code coevrage using the
[solidity-coverage](https://github.com/sc-forks/solidity-coverage) tool, run:

    ./node_modules/.bin/truffle run coverage test

You can then open the generated `coverage/index.html` to view the detailed
results.

### Contract Size

It can be useful to know the exact size of the compiled contracts (the
maximum size on the main network is 24 KiB = 24576 bytes). To see the size of
all the compiled contracts, use:

    ./node_modules/.bin/truffle run contract-size

This will invoke the
[truffle-contract-size](https://github.com/IoBuilders/truffle-contract-size)
plugin.

### Using Remix IDE

Using [Remix IDE](https://remix-project.org/) allows to directly call the smart
contracts functions.

- Start the test node (e.g. Ganache)
- Deploy the contracts using `yarn migrate:local` (check the URL and port
  number defined in `truffle.js` for the `dev_local` network match your test
  node)
- Start the Remix IDE (tested with the Remix desktop application)
- Extract the ABI of the contract you want to call and copy it into a file in
  Remix IDE.

The ABI is a JSON array describing the methods available in the smart contract
and their input and output parameters. It is part of the JSON file that is
output by the Solidity compiler. To extract the ABI, you can use `jq`:

    jq '.abi' build/contracts/ERC1400.json

Copy it into a file (e.g. `ERC1400.abi` in this case) in the Remix IDE.
![diagram](./docs/remix-ide-1-load-abi.png)

- Configure your test node as a "Web3 Provider"
- On the _Deploy & Run Transactions_ screen, check that `(abi)` is selected in
  _Contract_, enter the address of the smart contract and click on "At
  Address". All the contract functions should appear if you unfold the line
  corresponding to the contract in the _Deployed Contracts_ list.
  ![diagram](./docs/remix-ide-2-run-transactions.png)

## Smart Contracts Deployment

Deployment is normally done by the Java backend service, but it is possible to
deploy the smart contracts directly on a remote node.

Check the configuration defined for `parity_network` in `truffle.js`:

- If necessary, change the `host` parameter (you can leave the default value
  `0.0.0.0` if you are running the command directly on the node; if you are
  running the script from another host, put the IP address of the node you want
  to use).
- Set the `from` parameter to the address of the unlocked account of the
  deployment node

Then, open the file `migrations/2_erc_standard.js` and make sure the following
parameters are set to their correct values:

- `CERTIFICATE_SIGNER`: the address used to sign certificates accepted by the
  contract,
- `CONTROLLER`: the address of the controller,
- `OWNER`: address of the owner of the contract (corresponds to the issuer on
  the TGE platform),
- `DEFAULT_PARTITION`: the default partition used by contract methods that do
  not have a partition parameter (transfer...),
- `NAME`: name of the token (e.g. "Talium Dollar"),
- `SYMBOL`: symbol or code of the token (e.g. "TLD"),
- `DECIMALS`: define the number of decimals for the token (for display purpose
  only, the smart contract only stores the amount as unsigned integers).

To deploy the contracts, run `yarn run migrate:parity`.
