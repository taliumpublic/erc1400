// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.6.0 <0.8.0;

import "./ERC1400.sol";

contract StandaloneERC1400 is ERC1400 {
    constructor(
        string memory name,
        string memory symbol,
        uint8 decimals,
        bytes32 defaultPartition,
        address controller
    ) {
        initialize(name, symbol, decimals, defaultPartition, controller);
    }
}
