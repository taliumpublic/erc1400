// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.6.0 <0.8.0;

library ByteToString {
    function toAsciiHex(uint8 x) internal pure returns (bytes1) {
        if (x >= 0 && x < 10) {
            return bytes1(48 + x); // 0 must be mapped to '0' = 48
        } else {
            return bytes1(87 + x); // 10 must be mapped to 'a' = 97
        }
    }

    function byteToHexString(bytes1 b) internal pure returns (string memory) {
        bytes memory message = new bytes(2);
        uint8 value = uint8(b);
        message[0] = toAsciiHex((value & 0xf0) >> 4);
        message[1] = toAsciiHex(value & 0x0f);
        return string(message);
    }
}
