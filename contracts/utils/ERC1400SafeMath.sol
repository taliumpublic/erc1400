// SPDX-License-Identifier: MIT

pragma solidity >=0.6.0 <0.8.0;

/**
 * @dev This is a stripped down version of OpenZeppelin SafeMath library.
 * Only provides `add` and `sub`, and `add` contains a ESC-revert string to
 * indicate overflow.
 *
 * Refer to the original OpenZeppelin code for more details.
 */
library ERC1400SafeMath {
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "26"); // EIP-1066, 0x26, Above Range or Overflow

        return c;
    }

    function sub(
        uint256 a,
        uint256 b,
        string memory errorMessage
    ) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, "24");
    }

    /**
     * @notice Check if the sum causes an overflow.
     * @return `true` if the sum returns a correct result (no overflow)
     */
    function checkAdd(uint256 a, uint256 b) internal pure returns (bool) {
        uint256 c = a + b;
        return (c >= a);
    }
}
