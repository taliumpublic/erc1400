// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.6.0 <0.8.0;

import "openzeppelin-solidity/contracts/access/Ownable.sol";
import "openzeppelin-solidity/contracts/utils/Pausable.sol";
import "./ITransactionValidator.sol";
import "./CertificateVerifier.sol";

/**
 * If this contract is paused, this will effectively pause the ERC1400
 * contract.
 */
contract TransactionValidator is
    ITransactionValidator,
    Ownable,
    Pausable,
    CertificateVerifier
{
    address private _allowedCaller;

    // Can new tokens be issued?
    bool internal _issuable;

    constructor(address tokenContractAddress, address certificateSigner)
        Pausable()
        CertificateVerifier(certificateSigner)
    {
        _issuable = true;
        _allowedCaller = tokenContractAddress;
    }

    modifier onlyAllowedCaller() {
        require(
            _allowedCaller == msg.sender,
            "Delegate: caller is not the allowed caller"
        );
        _;
    }

    modifier onlyOwnerOrAllowedCaller() {
        require(
            owner() == msg.sender || _allowedCaller == msg.sender,
            "Delegate: caller is not the allowed caller"
        );
        _;
    }

    function allowedCaller() public view returns (address) {
        return _allowedCaller;
    }

    function setAllowedCaller(address newAllowedCaller) external onlyOwner {
        require(newAllowedCaller != address(0));
        _allowedCaller = newAllowedCaller;
    }

    /**
     * @notice Indicate if issuance (or "mint") is still possible on the token.
     * @return bool `true` means issuance is allowed. While `false` denotes the end of minting
     */
    function isIssuable() external view override returns (bool) {
        return _issuable;
    }

    function terminateIssuance() external onlyOwner {
        _issuable = false;
    }

    function revokeCertificateSigner(address account)
        external
        onlyOwner
        returns (bool)
    {
        return _revokeCertificateSigner(account);
    }

    function addCertificateSigner(address account) external onlyOwner {
        _addCertificateSigner(account);
    }

    function pause() external onlyOwner {
        _pause();
    }

    function unpause() external onlyOwner {
        _unpause();
    }

    /**
     * @notice Check certificate for a transaction.
     *
     * @param contractAddress Address of the smart contract the transaction is sent to
     * @param sender        Address sending the transaction
     * @param operation     Type of operation done in the transaction
     * @param data          The signed certificate.
     */
    function _verifySignatureAndCommonParams(
        address contractAddress,
        address sender,
        uint8 operation,
        bytes calldata data
    )
        private
        view
        returns (
            bytes1 status,
            address signerAddress,
            uint256 nonce,
            bytes memory operationParams
        )
    {
        if (data.length < 2) {
            return (0x00, address(0), uint256(0), bytes(""));
        }

        bytes memory certificate;
        (signerAddress, certificate) = verifySignature(data);

        if (signerAddress == address(0)) {
            // EIP-1066, 0xe0, Decrypt Failure
            return (0xe0, address(0), uint256(0), bytes(""));
        }

        // Separate the common parameters from the operation-specific parameters
        bytes memory certCommonParams;
        (certCommonParams, operationParams) = abi.decode(
            certificate,
            (bytes, bytes)
        );

        (status, nonce) = _checkCommonParamsAndGetNonce(
            contractAddress,
            sender,
            operation,
            signerAddress,
            certCommonParams
        );
    }

    function _checkCommonParamsAndGetNonce(
        address expectedTargetAddress,
        address expectedSender,
        uint8 expectedOperation,
        address signerAddress,
        bytes memory params
    ) private view returns (bytes1 status, uint256 nonce) {
        address targetAddress;
        uint256 validFrom;
        uint256 validTo;
        address sender;
        uint8 operation;

        // Decode the common parameters (and set nonce return value)
        // The parameters "version" (first) and "timestamp" (7th) are ignored
        (, targetAddress, nonce, validFrom, validTo, sender, , operation) = abi
            .decode(
            params,
            (uint8, address, uint256, uint256, uint256, address, uint64, uint8)
        );

        if (
            targetAddress != expectedTargetAddress ||
            operation != expectedOperation ||
            sender != expectedSender
        ) {
            // The target address (i.e. the address of the ERC1400 smart contract) must
            // be the address this validator is used by.
            // EIP-1066, 0xe4, Unsigned or Untrusted
            status = 0xe4;
        } else if (_isNonceUsed(signerAddress, nonce)) {
            // EIP-1066, 0x48, Already Done
            status = 0x48;
        } else if (validTo < block.timestamp) {
            // EIP-1066, 0x56, Expired
            status = 0x46;
        } else if (validFrom > block.timestamp) {
            // EIP-1006, 0x44, Not Available Yet
            status = 0x44;
        } else {
            // Success
            status = 0x01;
        }
    }

    /**
     *
     */
    function _matchTransferParameters(
        address expectedFrom,
        address expectedTo,
        bytes32 expectedPartitionFrom,
        uint256 expectedAmount,
        bytes memory params
    ) private pure returns (bool, bytes32) {
        (
            address from,
            address to,
            bytes32 partitionFrom,
            bytes32 partitionTo,
            uint256 amount
        ) = abi.decode(params, (address, address, bytes32, bytes32, uint256));

        return (
            from == expectedFrom &&
                to == expectedTo &&
                partitionFrom == expectedPartitionFrom &&
                amount == expectedAmount,
            partitionTo
        );
    }

    function _matchIssuanceParameters(
        address expectedTo,
        bytes32 expectedPartitionTo,
        uint256 expectedAmount,
        bytes memory params
    ) private pure returns (bool) {
        (address to, bytes32 partitionTo, uint256 amount) =
            abi.decode(params, (address, bytes32, uint256));

        return
            to == expectedTo &&
            partitionTo == expectedPartitionTo &&
            amount == expectedAmount;
    }

    function _matchRedemptionParameters(
        address expectedFrom,
        bytes32 expectedPartitionFrom,
        uint256 expectedAmount,
        bytes memory params
    ) private pure returns (bool) {
        (address from, bytes32 partitionFrom, uint256 amount) =
            abi.decode(params, (address, bytes32, uint256));

        return
            from == expectedFrom &&
            partitionFrom == expectedPartitionFrom &&
            amount == expectedAmount;
    }

    function checkTransfer(
        address sender,
        address from,
        address to,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    )
        external
        view
        override
        returns (
            bytes1 status,
            bytes32 reason,
            bytes32 partitionTo
        )
    {
        (status, reason, partitionTo, , ) = _checkTransfer(
            msg.sender,
            sender,
            from,
            to,
            partitionFrom,
            amount,
            data
        );
    }

    function checkTransfer2(
        address contractAddress,
        address sender,
        address from,
        address to,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    )
        public
        view
        returns (
            bytes1 status,
            bytes32 reason,
            bytes32 partitionTo
        )
    {
        (status, reason, partitionTo, , ) = _checkTransfer(
            contractAddress,
            sender,
            from,
            to,
            partitionFrom,
            amount,
            data
        );
    }

    function _checkTransfer(
        address contractAddress,
        address sender,
        address from,
        address to,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    )
        public
        view
        returns (
            bytes1 status,
            bytes32 reason,
            bytes32 partitionTo,
            address signerAddress,
            uint256 nonce
        )
    {
        if (paused()) {
            status = 0x42;
        } else {
            bytes memory params;

            (
                status,
                signerAddress,
                nonce,
                params
            ) = _verifySignatureAndCommonParams(
                contractAddress,
                sender,
                0,
                data
            );

            if (status == 0x01) {
                // Check if the transfer parameters passed to the function
                // match the ones encoded in the certificate
                bool paramsMatch;

                (paramsMatch, partitionTo) = _matchTransferParameters(
                    from,
                    to,
                    partitionFrom,
                    amount,
                    params
                );

                if (!paramsMatch) {
                    status = 0x08;
                }
            }
        }
    }

    function validateTransfer(
        address sender,
        address from,
        address to,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    ) external override onlyAllowedCaller returns (bytes32 partitionTo) {
        require(!paused(), "42");

        bytes1 status;
        address signerAddress;
        uint256 nonce;

        (status, , partitionTo, signerAddress, nonce) = _checkTransfer(
            msg.sender,
            sender,
            from,
            to,
            partitionFrom,
            amount,
            data
        );

        if (status == 0x01) {
            // Certificate is valid for the operation, mark the nonce as used
            _recordNonceUsed(signerAddress, nonce);
        } else {
            revert(string(ByteToString.byteToHexString(status)));
        }
    }

    function validateIssuance(
        address sender,
        address to,
        bytes32 partitionTo,
        uint256 amount,
        bytes calldata data
    ) external override onlyAllowedCaller {
        require(!paused(), "42");
        require(_issuable, "18"); // EIP-1066, 0x18, Not Applicable to Current State

        (
            bytes1 status,
            address signerAddress,
            uint256 nonce,
            bytes memory params
        ) = _verifySignatureAndCommonParams(msg.sender, sender, 2, data);

        if (status != 0x01) {
            revert(ByteToString.byteToHexString(status));
        } else if (!_matchIssuanceParameters(to, partitionTo, amount, params)) {
            revert("08");
        }

        // Success, mark nonce as used
        _recordNonceUsed(signerAddress, nonce);
    }

    function validateRedemption(
        address sender,
        address from,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    ) external override onlyAllowedCaller {
        require(!paused(), "42");

        (
            bytes1 status,
            address signerAddress,
            uint256 nonce,
            bytes memory params
        ) = _verifySignatureAndCommonParams(msg.sender, sender, 1, data);

        if (status != 0x01) {
            revert(ByteToString.byteToHexString(status));
        } else if (
            !_matchRedemptionParameters(from, partitionFrom, amount, params)
        ) {
            revert("08");
        }

        // Success, mark nonce as used
        _recordNonceUsed(signerAddress, nonce);
    }
}
