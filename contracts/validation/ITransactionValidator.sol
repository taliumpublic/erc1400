// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.6.0 <0.8.0;

import "openzeppelin-solidity/contracts/utils/Pausable.sol";

/**
 * @title Validation of transactions for the ERC1400 smart contract.
 *
 * The ERC1400 smart contract calls `checkTransaction` or `validateTransaction`
 */
interface ITransactionValidator {
    /**
     * @notice Check if a transaction should be accepted.
     *
     * @return status   Status code (ESC, see EIP-1066)
     * @return reason   Additional reason code.
     * @return partitionTo Destination partition.
     */
    function checkTransfer(
        address sender,
        address from,
        address to,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    )
        external
        view
        returns (
            bytes1 status,
            bytes32 reason,
            bytes32 partitionTo
        );

    /**
     * @notice Validate a transaction (revert if transaction is not accepted).
     *
     * @dev It should be only possible to use a certificate (`data`) once, the
     * implementation of this function should make sure that a certificate cannot
     * be used to validate the same transaction twice (unkike `checkTransaction`
     * which does not make the certificate invalid for future use).
     */
    function validateTransfer(
        address sender,
        address from,
        address to,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    ) external returns (bytes32 partitionTo);

    function validateIssuance(
        address sender,
        address to,
        bytes32 partitionTo,
        uint256 amount,
        bytes calldata data
    ) external;

    function validateRedemption(
        address sender,
        address from,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    ) external;

    function isIssuable() external view returns (bool);
}
