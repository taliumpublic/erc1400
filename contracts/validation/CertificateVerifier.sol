// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.6.0 <0.8.0;

import "openzeppelin-solidity/contracts/cryptography/ECDSA.sol";
import "../utils/ByteToString.sol";

/*
 * The `data` parameter passed to the different ERC1400 functions is used to
 * prove the transaction is allowed by the off-chain rules.
 *
 * This contract validates the `data` parameter by checking it is signed by an
 * authorized "signer" (multiple authorized signers can be defined).
 */
contract CertificateVerifier {
    // Addresses allowed to sign the certificates
    mapping(address => bool) private _trustedSigners;

    // Used nonces (per certificate signer address)
    mapping(address => mapping(uint256 => bool)) private _nonces;

    constructor(address signer) {
        // Add initial certificate signer
        _trustedSigners[signer] = true;
    }

    function isCertificateSigner(address account) public view returns (bool) {
        return _trustedSigners[account];
    }

    function _revokeCertificateSigner(address account) internal returns (bool) {
        if (_trustedSigners[account]) {
            _trustedSigners[account] = false;
            return true;
        } else {
            return false;
        }
    }

    function _addCertificateSigner(address account) internal {
        require(account != address(0), "20");
        _trustedSigners[account] = true;
    }

    function _recordNonceUsed(address signer, uint256 nonce) internal {
        _nonces[signer][nonce] = true;
    }

    function _isNonceUsed(address signer, uint256 nonce)
        internal
        view
        returns (bool)
    {
        return _nonces[signer][nonce];
    }

    /**
     * @notice Verify the certificate signature
     *
     * @dev This function does not really need to be accessible publicly, but
     * it causes no harm and makes testing easier.
     */
    function verifySignature(bytes calldata data)
        public
        view
        returns (address signerAddress, bytes memory certificate)
    {
        // First, separate the certificate from the signature
        bytes memory signature;
        (certificate, signature) = abi.decode(data, (bytes, bytes));

        signerAddress = extractSignerAddress(certificate, signature);

        if (!_trustedSigners[signerAddress]) {
            signerAddress = address(0);
        }
    }

    /**
     * @notice Get the address corresponding to the key used to sign the
     * certificate.
     *
     * @dev This function does not really need to be accessible publicly, but
     * it causes no harm and makes testing easier.
     *
     * @param certificate   Certificate provided for the transaction
     * @param signature     Signature of the certificate
     * @return              Address of the certificate signer
     */
    function extractSignerAddress(
        bytes memory certificate,
        bytes memory signature
    ) public pure returns (address) {
        bytes32 hash =
            keccak256(
                abi.encodePacked(
                    "\x19Ethereum Signed Message:\n32",
                    keccak256(certificate)
                )
            );

        return ECDSA.recover(hash, signature);
    }
}
