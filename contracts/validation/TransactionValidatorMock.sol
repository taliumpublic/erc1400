// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.6.0 <0.8.0;

import "./ITransactionValidator.sol";

contract TransactionValidatorMock is ITransactionValidator {
    function checkTransfer(
        address sender,
        address from,
        address to,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    )
        external
        view
        override
        returns (
            bytes1 status,
            bytes32 reason,
            bytes32 partitionTo
        )
    {
        return (0x01, bytes32(""), partitionFrom);
    }

    function validateTransfer(
        address sender,
        address from,
        address to,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    ) external override returns (bytes32 partitionTo) {
        return partitionFrom;
    }

    function validateIssuance(
        address sender,
        address to,
        bytes32 partitionTo,
        uint256 amount,
        bytes calldata data
    ) external override {}

    function validateRedemption(
        address sender,
        address from,
        bytes32 partitionFrom,
        uint256 amount,
        bytes calldata data
    ) external override {}

    function isIssuable() external view override returns (bool) {
        return true;
    }
}
