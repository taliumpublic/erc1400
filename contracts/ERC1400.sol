// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.6.0 <0.8.0;

import "openzeppelin-solidity/contracts/token/ERC20/IERC20.sol";
import "openzeppelin-solidity/contracts/proxy/Initializable.sol";
import "./interfaces/IERC1410.sol";
import "./interfaces/IERC1643.sol";
import "./interfaces/IERC1594.sol";
import "./interfaces/IERC1644.sol";
import "./validation/ITransactionValidator.sol";
import "./utils/ERC1400SafeMath.sol";

/**
 * @title ERC1400 Implementation.
 *
 * @dev Note this contract is meant to be used with the OpenZeppelin
 * transparent proxy, it does not include a constructor but an initializer. To
 * use the contract and set the parameters at deployment time use the
 * `StandaloneERC1400` contract which provides a constructor.
 *
 * To be able to upgrade a contract using a transparent proxy, the storage must
 * be compatible with the previous version of the contract, that means that if
 * you modify this contract, existing state variables cannot be removed and new
 * state variables can only be added at the end. Be careful with inheritance.
 *
 * See https://docs.openzeppelin.com/upgrades-plugins/1.x/proxies for more
 * details.
 */
contract ERC1400 is
    IERC1643,
    IERC1644,
    IERC1594,
    IERC1410,
    IERC20,
    Initializable
{
    using ERC1400SafeMath for uint256;

    string private _name;

    string private _symbol;

    uint8 private _decimals;

    uint256 private _totalSupply;

    // The default partition is used as a default for operations that do not specify
    // a partition explicitly (ERC20 transfer(), transferWithData()...)
    bytes32 private _defaultPartition;

    // Mapping from investor to aggregated balance across all investor token sets
    mapping(address => uint256) _balances;

    mapping(address => mapping(bytes32 => uint256))
        private _balancesByPartition;

    mapping(address => bytes32[]) private _partitions;

    // Index of the partitions in the `_partitions` arrays
    mapping(address => mapping(bytes32 => uint256)) private _partitionsIndex;

    // Mapping from (investor, partition, operator) to approved status
    mapping(address => mapping(bytes32 => mapping(address => bool)))
        private _operatorsByPartition;

    mapping(address => mapping(address => bool)) private _operators;

    // Allowances checked by transferFrom() and transferFromWithData() (but
    // not redeem functions like redeemFrom())
    // If _allowances[owner][spender] = amount, this means `spender` is allowed
    // to spend `amount' tokens from the tokens belonging to `owner`.
    mapping(address => mapping(address => uint256)) private _allowances;

    // Is the contract controllable?
    bool private _controllable;

    mapping(address => bool) private _controllers;

    // Contract owner
    address private _owner;

    struct Document {
        bytes32 docHash; // Hash of the document
        uint256 lastModified; // Timestamp at which document details was last modified
        string uri; // URI of the document (off-chain)
    }

    // Documents information
    mapping(bytes32 => Document) private _documents;

    // mapping to store the document name indexes
    mapping(bytes32 => uint256) private _documentIndices;

    // Array use to store all the document names present in the contract
    bytes32[] private _documentNames;

    ITransactionValidator private _transactionValidator;

    /**
     * @notice Initializer.
     */
    function initialize(
        string memory name_,
        string memory symbol_,
        uint8 decimals_,
        bytes32 defaultPartition,
        address controller
    ) public initializer {
        _owner = msg.sender;
        _name = name_;
        _symbol = symbol_;
        _decimals = decimals_;
        _defaultPartition = defaultPartition;
        _controllable = true;
        _controllers[controller] = true;
    }

    /**
     * @dev Returns the address of the current contract owner.
     */
    function owner() external view returns (address) {
        return _owner;
    }

    function requireOnlyOwner() private view {
        require(_owner == msg.sender, "10");
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) external {
        requireOnlyOwner();
        _owner = newOwner;
    }

    function getTransactionValidator() external view returns (address) {
        return address(_transactionValidator);
    }

    function configure(address transactionValidator) external {
        requireOnlyOwner();
        _transactionValidator = ITransactionValidator(transactionValidator);
    }

    /**
     * @notice Indicate if it is possible for a controller to perform operations.
     * @return bool `true` when token is controllable otherwise return `false`.
     */
    function isControllable() external view override returns (bool) {
        return _controllable;
    }

    function terminateControl() external {
        requireOnlyOwner();
        _controllable = false;
    }

    function isController(address account) external view returns (bool) {
        return _controllers[account];
    }

    function revokeController(address account) external {
        requireOnlyOwner();
        if (_controllers[account]) {
            _controllers[account] = false;
        }
    }

    function addController(address account) external {
        requireOnlyOwner();
        require(account != address(0), "20");
        _controllers[account] = true;
    }

    /**
     * @notice Indicate if issuance (or "mint") is still possible on the token.
     */
    function isIssuable() external view override returns (bool) {
        return _transactionValidator.isIssuable();
    }

    function name() external view returns (string memory) {
        return _name;
    }

    function symbol() external view returns (string memory) {
        return _symbol;
    }

    function decimals() external view returns (uint8) {
        return _decimals;
    }

    function getDefaultPartition() external view returns (bytes32) {
        return _defaultPartition;
    }

    function setDefaultPartition(bytes32 newDefaultPartition) external {
        requireOnlyOwner();
        _defaultPartition = newDefaultPartition;
    }

    // Operator management

    /**
     * @notice Authorises an operator for all partitions of `msg.sender`
     *
     * @param operator  Operator address
     */
    function authorizeOperator(address operator) external override {
        require(operator != msg.sender, "20");
        _operators[msg.sender][operator] = true;
        emit AuthorizedOperator(operator, msg.sender);
    }

    /**
     * @notice Authorises an operator for a given partition of `msg.sender`
     *
     * @param partition Partition the operator is to be authorized on
     * @param operator  Operator address
     */
    function authorizeOperatorByPartition(bytes32 partition, address operator)
        external
        override
    {
        require(operator != msg.sender, "20");
        _operatorsByPartition[msg.sender][partition][operator] = true;
        emit AuthorizedOperatorByPartition(partition, operator, msg.sender);
    }

    /**
     * @notice Revokes authorisation of an operator previously given for all
     * partitions of `msg.sender`
     *
     * @param operator  Operator address
     */
    function revokeOperator(address operator) external override {
        _operators[msg.sender][operator] = false;
        emit RevokedOperator(operator, msg.sender);
    }

    /**
     * @notice Determines whether `operator` is an operator for all partitions
     * of `account`
     *
     * @param operator  Operator address
     * @param account   Account address
     *
     * @return          Returns `true` if `operator` is an operator for all
     * partitions of `account`
     */
    function isOperator(address operator, address account)
        public
        view
        override
        returns (bool)
    {
        return _operators[account][operator];
    }

    /**
     * @notice Revokes authorisation of an operator previously given for a
     * specified partition of `msg.sender`
     *
     * @param partition Partition the operator is to be de-authorized on
     * @param operator  Operator address
     */
    function revokeOperatorByPartition(bytes32 partition, address operator)
        external
        override
    {
        _operatorsByPartition[msg.sender][partition][operator] = false;
        emit RevokedOperatorByPartition(partition, operator, msg.sender);
    }

    /**
     * @notice Check if an address is an operator on behalf of `account` for
     * a given partition.
     *
     * @param partition Partition
     * @param operator  Operator address
     * @param account   Account address
     *
     * @return          Returns `true` if `operator` has operator authorization
     * on partition for the given `account`.
     */
    function isOperatorForPartition(
        bytes32 partition,
        address operator,
        address account
    ) public view override returns (bool) {
        return
            _operators[account][operator] ||
            _operatorsByPartition[account][partition][operator];
    }

    /**
     * @notice Check the caller is allowed to call an "operator" function.
     *
     * @dev This is used for `operatorTransferByPartition` and
     * `operatorRedeemByPartition`, which can be called either by an operator
     * or by a controller.
     */
    function _checkOperatorAccess(bytes32 partition, address account)
        private
        view
        returns (bool)
    {
        bool isSenderController = _controllable && _controllers[msg.sender];

        require(
            msg.sender == account ||
                isOperatorForPartition(partition, msg.sender, account) ||
                isSenderController,
            "10" // EIP-1066, 0x10, Disallowed
        );

        return isSenderController;
    }

    // Balance and partitions management

    function totalSupply()
        external
        view
        override(IERC20, IERC1410)
        returns (uint256)
    {
        return _totalSupply;
    }

    function balanceOf(address account)
        external
        view
        override(IERC20, IERC1410)
        returns (uint256)
    {
        return _balances[account];
    }

    /**
     * @notice Retrieve account balance for a given partition.
     *
     * @param partition Partition
     * @param account   Account address
     *
     * @return          Number of tokens owned by `account`on the partition
     * `partition`.
     */
    function balanceOfByPartition(bytes32 partition, address account)
        external
        view
        override
        returns (uint256)
    {
        return _balancesByPartition[account][partition];
    }

    /**
     * @notice Get the list of partitions `account` has tokens on.
     *
     * @param account   Account address
     *
     * @return          List of partitions
     */
    function partitionsOf(address account)
        external
        view
        override
        returns (bytes32[] memory)
    {
        return _partitions[account];
    }

    function _addToPartition(
        bytes32 partition,
        address account,
        uint256 amount
    ) internal {
        require(account != address(0) && amount != 0, "20"); // EIP-1066, 0x20, Not Found, Unequal, or Out of Range

        uint256 current = _balancesByPartition[account][partition];
        _balancesByPartition[account][partition] = current.add(amount);
        _balances[account] = _balances[account].add(amount);

        if (current == 0) {
            // New partition for this account, add it to the array
            // Record the index to be able to delete it later
            _partitions[account].push(partition);
            _partitionsIndex[account][partition] = _partitions[account].length;
        }
    }

    function _subtractFromPartition(
        bytes32 partition,
        address account,
        uint256 amount
    ) internal {
        require(account != address(0) && amount != 0, "20"); // EIP-1066, 0x20, Not Found, Unequal, or Out of Range

        // On underflow: EIP-1066, 0x54, Insufficient Funds
        _balancesByPartition[account][partition] = _balancesByPartition[
            account
        ][partition]
            .sub(amount, "54");
        _balances[account] = _balances[account].sub(amount, "54");

        if (_balancesByPartition[account][partition] == 0) {
            // Partition is empty for this account, remove it from `_partitions`
            // We can only remove ("pop") the last element of the array
            uint256 partitionIndex = _partitionsIndex[account][partition] - 1;
            if (partitionIndex < _partitions[account].length - 1) {
                // The element we want to remove is not at the end of the array,
                // move the last element to the location of the element
                _partitions[account][partitionIndex] = _partitions[account][
                    _partitions[account].length - 1
                ];
            }
            _partitions[account].pop();
        }
    }

    function _approve(
        address account,
        address spender,
        uint256 amount
    ) internal {
        require(account != address(0) && spender != address(0), "20"); // EIP-1066, 0x20, Not Found, Unequal, or Out of Range

        _allowances[account][spender] = amount;
        emit Approval(account, spender, amount);
    }

    function approve(address spender, uint256 amount)
        external
        override
        returns (bool)
    {
        _approve(msg.sender, spender, amount);
        return true;
    }

    function allowance(address account, address spender)
        external
        view
        override
        returns (uint256)
    {
        return _allowances[account][spender];
    }

    function increaseAllowance(address spender, uint256 amount) external {
        _approve(
            msg.sender,
            spender,
            _allowances[msg.sender][spender].add(amount)
        );
    }

    function decreaseAllowance(address spender, uint256 amount) external {
        _approve(
            msg.sender,
            spender,
            _allowances[msg.sender][spender].sub(amount)
        );
    }

    function _transfer(
        address sender,
        address from,
        address to,
        bytes32 partitionFrom,
        uint256 amount,
        bytes memory data,
        bytes memory operatorData
    ) internal returns (bytes32 partitionTo) {
        partitionTo = _transactionValidator.validateTransfer(
            sender,
            from,
            to,
            partitionFrom,
            amount,
            data
        );

        _subtractFromPartition(partitionFrom, from, amount);
        _addToPartition(partitionTo, to, amount);

        emit TransferByPartition(
            partitionFrom,
            sender,
            from,
            to,
            amount,
            data,
            operatorData
        );

        // ERC-20 compatibility (do not emit ERC-20 transfer event in case of
        // change of partition only)
        if (from != to) {
            emit Transfer(from, to, amount);
        }

        return partitionTo;
    }

    function _canTransfer(
        address sender,
        address from,
        address to,
        bytes32 partitionFrom,
        uint256 amount,
        bytes memory data
    )
        internal
        view
        returns (
            bytes1 status,
            bytes32 reason,
            bytes32 partitionTo
        )
    {
        (status, reason, partitionTo) = _transactionValidator.checkTransfer(
            sender,
            from,
            to,
            partitionFrom,
            amount,
            data
        );

        if (from == address(0) || to == address(0) || amount == 0) {
            status = 0x20;
        } else if (status == 0x01 || status == 0x03) {
            // Transaction is valid, check balances/allowances
            uint256 balanceFrom = _balancesByPartition[from][partitionFrom];
            uint256 balanceTo = _balancesByPartition[to][partitionTo];

            if (balanceFrom < amount) {
                // EIP-1066, 0x54, Insufficient Funds
                status = 0x54;
            } else if (!ERC1400SafeMath.checkAdd(balanceTo, amount)) {
                // EIP-1066, 0x26, Above Range or Overflow
                status = 0x26;
            } else if (sender != from && amount > _allowances[from][sender]) {
                // EIP-1066, 0x56, Transfer Volume Exceeded
                status = 0x56;
            }
        }
    }

    function _issue(
        address sender,
        address to,
        bytes32 partition,
        uint256 amount,
        bytes memory data
    ) internal {
        _transactionValidator.validateIssuance(
            sender,
            to,
            partition,
            amount,
            data
        );

        _addToPartition(partition, to, amount);
        _totalSupply = _totalSupply.add(amount);

        emit IssuedByPartition(partition, to, amount, data);
        emit Issued(sender, to, amount, data);
        emit Transfer(address(0), to, amount);
    }

    function _redeem(
        address sender,
        address from,
        bytes32 partition,
        uint256 amount,
        bytes memory data,
        bytes memory operatorData
    ) internal {
        _transactionValidator.validateRedemption(
            sender,
            from,
            partition,
            amount,
            data
        );

        _subtractFromPartition(partition, from, amount);
        _totalSupply = _totalSupply.sub(amount);

        emit RedeemedByPartition(
            partition,
            sender,
            from,
            amount,
            data,
            operatorData
        );
        emit Redeemed(sender, from, amount, data);
        emit Transfer(from, address(0), amount);
    }

    function transfer(address to, uint256 amount)
        external
        override
        returns (bool)
    {
        _transfer(
            msg.sender, // Sender
            msg.sender, // From
            to,
            _defaultPartition,
            amount,
            "",
            ""
        );
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) external override returns (bool) {
        require(amount <= _allowances[from][msg.sender], "56"); // EIP-1066, 0x56, Transfer Volume Exceeded

        _transfer(
            msg.sender, // Sender
            from, // From
            to,
            _defaultPartition,
            amount,
            "",
            ""
        );
        _approve(from, msg.sender, _allowances[from][msg.sender].sub(amount));
        return true;
    }

    /**
     * @notice Transfer tokens.
     *
     * @param partition Source (and destination) partition
     * @param to        Destination account address
     * @param amount    Amount of tokens to transfer
     * @param data      Certificate
     *
     * @return          Destination partition
     */
    function transferByPartition(
        bytes32 partition,
        address to,
        uint256 amount,
        bytes calldata data
    ) external override returns (bytes32) {
        return
            _transfer(msg.sender, msg.sender, to, partition, amount, data, "");
    }

    /**
     * @notice Transfer tokens (operator).
     *
     * @dev This function can only be called by the owner of the tokens, by an
     * operator authorized by the owner, or by a controller.
     *
     * @param partition Source partition
     * @param from      The address from which to transfer tokens from
     * @param to        The address to which to transfer tokens to
     * @param amount    Amount of tokens to transfer
     * @param data      Certificate
     * @param operatorData Unused (but included in the generated events)
     *
     * @return partitionTo The partition to which the transferred tokens were
     * allocated for the to address
     */
    function operatorTransferByPartition(
        bytes32 partition,
        address from,
        address to,
        uint256 amount,
        bytes calldata data,
        bytes calldata operatorData
    ) external override returns (bytes32 partitionTo) {
        // Check if the caller is allowed to invoke the method, revert if not
        // Returns `true` if the caller is a controller.
        bool doTransferAsController = _checkOperatorAccess(partition, from);

        partitionTo = _transfer(
            msg.sender,
            from,
            to,
            partition,
            amount,
            data,
            operatorData
        );

        if (doTransferAsController) {
            emit ControllerTransfer(
                msg.sender,
                from,
                to,
                amount,
                data,
                operatorData
            );
        }
    }

    /**
     * @notice Check if the transfer (`transferWithData`) will succeed.
     *
     * @param to        Source account address
     * @param amount    Destination account address
     * @param data      Certificate
     *
     * @return status   ESC (EIP-1066 Ethereum Status Code)
     * @return reason   Application specific reason code
     */
    function canTransfer(
        address to,
        uint256 amount,
        bytes calldata data
    ) external view override returns (bytes1 status, bytes32 reason) {
        (status, reason, ) = _canTransfer(
            msg.sender,
            msg.sender,
            to,
            _defaultPartition,
            amount,
            data
        );
    }

    /**
     * @notice Check if the transfer (`transferFromWithData`) will succeed.
     *
     * @param from      Source account address
     * @param to        Destination account address
     * @param amount    Amount of tokens to transfer
     * @param data      Certificate.
     *
     * @return status   ESC (EIP-1066 Ethereum Status Code)
     * @return reason   Application specific reason code
     */
    function canTransferFrom(
        address from,
        address to,
        uint256 amount,
        bytes calldata data
    ) external view override returns (bytes1 status, bytes32 reason) {
        (status, reason, ) = _canTransfer(
            msg.sender,
            from,
            to,
            _defaultPartition,
            amount,
            data
        );
    }

    /**
     * @notice Check if the transfer (`transferByPartition`) will succeed.
     *
     * @param from      Source account address
     * @param to        Destination account address
     * @param partition Source partition
     * @param amount    Amount of tokens to transfer
     * @param data      Certificate
     *
     * @return status   ESC (EIP-1066 Ethereum Status Code)
     * @return reason   Application specific reason codes with additional details
     * @return partitionTo Destination partition
     */
    function canTransferByPartition(
        address from,
        address to,
        bytes32 partition,
        uint256 amount,
        bytes calldata data
    )
        external
        view
        override
        returns (
            bytes1 status,
            bytes32 reason,
            bytes32 partitionTo
        )
    {
        // Since we want to find out if `transferByPartition` would work, we
        // consider the sender address to be the same as the "from" address
        return _canTransfer(from, from, to, partition, amount, data);
    }

    /**
     * @notice Transfer tokens.
     *
     * @param to        Destination account address
     * @param amount    Amount of tokens to transfer
     * @param data      Certificate
     */
    function transferWithData(
        address to,
        uint256 amount,
        bytes calldata data
    ) external override {
        _transfer(
            msg.sender,
            msg.sender,
            to,
            _defaultPartition,
            amount,
            data,
            ""
        );
    }

    /**
     * @notice Transfer tokens.
     *
     * @dev The caller (`msg.sender`) must have been allowed by the owner of
     * tokens (`from`) to transfer tokens.
     *
     * @param from      Source account address
     * @param to        Destination account address
     * @param amount    Amount of tokens to be transferred
     * @param data      Certificate
     */
    function transferFromWithData(
        address from,
        address to,
        uint256 amount,
        bytes calldata data
    ) external override {
        require(amount <= _allowances[from][msg.sender], "56"); // EIP-1066, 0x56, Transfer Volume Exceeded

        _transfer(msg.sender, from, to, _defaultPartition, amount, data, "");
        _approve(from, msg.sender, _allowances[from][msg.sender].sub(amount));
    }

    /**
     * @notice Issue ("mint") new tokens.
     *
     * @param to        Account that will receive the issued tokens
     * @param amount    Amount of tokens to be issued
     * @param data      Certificate.
     */
    function issue(
        address to,
        uint256 amount,
        bytes calldata data
    ) external override {
        _issue(msg.sender, to, _defaultPartition, amount, data);
    }

    /**
     * @notice Issue ("mint") new tokens.
     *
     * @param partition The partition on which the tokens should be issued
     * @param to        The account owning the issued tokens
     * @param amount    The amount by which to increase the balance
     * @param data      Certificate
     */
    function issueByPartition(
        bytes32 partition,
        address to,
        uint256 amount,
        bytes calldata data
    ) external override {
        _issue(msg.sender, to, partition, amount, data);
    }

    /**
     * @notice Redeem ("burn") tokens belonging to the caller.
     *
     * @param partition The partition from which the tokens should be redeemed
     * @param amount    The amount of tokens to be redeemed
     * @param data      Certificate
     */
    function redeemByPartition(
        bytes32 partition,
        uint256 amount,
        bytes calldata data
    ) external override {
        _redeem(msg.sender, msg.sender, partition, amount, data, "");
    }

    /**
     * @notice Redeem ("burn") tokens belonging to the caller.
     *
     * @param amount        The amount of tokens to be redeemed
     * @param data          Certificate
     */
    function redeem(uint256 amount, bytes calldata data) external override {
        _redeem(msg.sender, msg.sender, _defaultPartition, amount, data, "");
    }

    /**
     * @notice Redeem ("burn") tokens belonging to the caller.
     *
     * @param from          The account whose tokens gets redeemed.
     * @param amount        The amount of tokens to be redeemed
     * @param data          Certificate
     */
    function redeemFrom(
        address from,
        uint256 amount,
        bytes calldata data
    ) external override {
        _redeem(msg.sender, from, _defaultPartition, amount, data, "");
    }

    /**
     * @notice Redeem ("burn") tokens.
     *
     * @dev This function can only be called by the owner of the tokens, by an
     * operator authorized by the owner, or by a controller.
     *
     * @param partition The partition they tokens should be redeemed from.
     * @param from      The token holder whose balance should be decreased
     * @param amount    Amount of tokens to redeem
     * @param data      Certificate
     * @param operatorData Unused (but included in the generated events)
     */
    function operatorRedeemByPartition(
        bytes32 partition,
        address from,
        uint256 amount,
        bytes calldata data,
        bytes calldata operatorData
    ) external override {
        bool doRedeemAsController = _checkOperatorAccess(partition, from);
        _redeem(msg.sender, from, partition, amount, data, operatorData);

        if (doRedeemAsController) {
            emit ControllerRedemption(
                msg.sender,
                from,
                amount,
                data,
                operatorData
            );
        }
    }

    // ERC1643 Document Management
    // Only the owner is allowed to add or update documents

    function getDocument(bytes32 documentName)
        public
        view
        override
        returns (
            string memory uri,
            bytes32 docHash,
            uint256 lastModified
        )
    {
        require(documentName != bytes32(0), "20");
        return (
            _documents[documentName].uri,
            _documents[documentName].docHash,
            _documents[documentName].lastModified
        );
    }

    function setDocument(
        bytes32 documentName,
        string calldata uri,
        bytes32 documentHash
    ) external override {
        requireOnlyOwner();
        require(documentName != bytes32(0) && bytes(uri).length > 0, "20");

        if (_documents[documentName].lastModified == uint256(0)) {
            _documentNames.push(documentName);
            _documentIndices[documentName] = _documentNames.length;
        }
        _documents[documentName] = Document(documentHash, block.timestamp, uri);
        emit DocumentUpdated(documentName, uri, documentHash);
    }

    function removeDocument(bytes32 documentName) external override {
        requireOnlyOwner();

        (string memory uri, bytes32 documentHash, ) = getDocument(documentName);
        require(_documents[documentName].lastModified != uint256(0), "20");

        uint256 index = _documentIndices[documentName] - 1;
        if (index != _documentNames.length - 1) {
            _documentNames[index] = _documentNames[_documentNames.length - 1];
            _documentIndices[_documentNames[index]] = index + 1;
        }
        _documentNames.pop();
        delete _documents[documentName];

        emit DocumentRemoved(documentName, uri, documentHash);
    }

    function getAllDocuments()
        external
        view
        override
        returns (bytes32[] memory)
    {
        return _documentNames;
    }
}
