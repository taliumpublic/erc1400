// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.6.0 <0.8.0;

/**
 * @title ERC 1410: Partially Fungible Token Standard
 * @dev See https://github.com/ethereum/EIPs/issues/1410
 */
interface IERC1410 {
    // Events

    // Operator Events
    event AuthorizedOperator(
        address indexed _operator,
        address indexed _tokenHolder
    );

    event RevokedOperator(
        address indexed _operator,
        address indexed _tokenHolder
    );

    event AuthorizedOperatorByPartition(
        bytes32 indexed _partition,
        address indexed _operator,
        address indexed _tokenHolder
    );

    event RevokedOperatorByPartition(
        bytes32 indexed _partition,
        address indexed _operator,
        address indexed _tokenHolder
    );

    // Issuance / Redemption Events
    event IssuedByPartition(
        bytes32 indexed _partition,
        address indexed _to,
        uint256 _value,
        bytes _data
    );

    event RedeemedByPartition(
        bytes32 indexed _partition,
        address indexed _operator,
        address indexed _from,
        uint256 _value,
        bytes _data,
        bytes _operatorData
    );

    // Transfer Events
    event TransferByPartition(
        bytes32 indexed _fromPartition,
        address _operator,
        address indexed _from,
        address indexed _to,
        uint256 _value,
        bytes _data,
        bytes _operatorData
    );

    event ChangedPartition(
        bytes32 indexed _fromPartition,
        bytes32 indexed _toPartition,
        uint256 _value
    );

    // Functions

    // Token Information
    function balanceOf(address _tokenHolder) external view returns (uint256);

    function balanceOfByPartition(bytes32 _partition, address _tokenHolder)
        external
        view
        returns (uint256);

    function partitionsOf(address _tokenHolder)
        external
        view
        returns (bytes32[] memory);

    function totalSupply() external view returns (uint256);

    // Token Transfers
    function transferByPartition(
        bytes32 _partition,
        address _to,
        uint256 _value,
        bytes calldata _data
    ) external returns (bytes32);

    function operatorTransferByPartition(
        bytes32 _partition,
        address _from,
        address _to,
        uint256 _value,
        bytes calldata _data,
        bytes calldata _operatorData
    ) external returns (bytes32);

    function canTransferByPartition(
        address _from,
        address _to,
        bytes32 _partition,
        uint256 _value,
        bytes calldata _data
    )
        external
        view
        returns (
            bytes1,
            bytes32,
            bytes32
        );

    // Operator Information
    function isOperator(address _operator, address _tokenHolder)
        external
        view
        returns (bool);

    function isOperatorForPartition(
        bytes32 _partition,
        address _operator,
        address _tokenHolder
    ) external view returns (bool);

    // Operator Management
    function authorizeOperator(address _operator) external;

    function revokeOperator(address _operator) external;

    function authorizeOperatorByPartition(bytes32 _partition, address _operator)
        external;

    function revokeOperatorByPartition(bytes32 _partition, address _operator)
        external;

    // Issuance / Redemption
    function issueByPartition(
        bytes32 _partition,
        address _tokenHolder,
        uint256 _value,
        bytes calldata _data
    ) external;

    function redeemByPartition(
        bytes32 _partition,
        uint256 _value,
        bytes calldata _data
    ) external;

    function operatorRedeemByPartition(
        bytes32 _partition,
        address _tokenHolder,
        uint256 _value,
        bytes calldata _data,
        bytes calldata _operatorData
    ) external;
}
