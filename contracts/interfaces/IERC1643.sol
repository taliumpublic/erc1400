// SPDX-License-Identifier: UNLICENSED
pragma solidity >=0.6.0 <0.8.0;

/**
 * @title ERC 1643: Document Management Standard
 * @dev See https://github.com/ethereum/EIPs/issues/1643
 */
interface IERC1643 {
    event DocumentRemoved(
        bytes32 indexed _name,
        string _uri,
        bytes32 _documentHash
    );

    event DocumentUpdated(
        bytes32 indexed _name,
        string _uri,
        bytes32 _documentHash
    );

    function getDocument(bytes32 _name)
        external
        view
        returns (
            string memory,
            bytes32,
            uint256
        );

    function setDocument(
        bytes32 _name,
        string calldata _uri,
        bytes32 _documentHash
    ) external;

    function removeDocument(bytes32 _name) external;

    function getAllDocuments() external view returns (bytes32[] memory);
}
