const ERC1400 = artifacts.require("./StandaloneERC1400.sol");
const TransactionValidator = artifacts.require(
  "./validation/TransactionValidator.sol"
);

module.exports = async function (deployer) {
  // Replace addresses with the correct ones
  const CERTIFICATE_SIGNER = "0xDe826Ea7c3841e3F7d7CD1399FEcDE4fD107CE54";
  const CONTROLLER = "0xEab8c96C8c8663FEd8480A1De5c5701eDCEDb087"; // 2
  const OWNER = "0x772a4694EE9a83855267606e890B37Cd9682685E"; // 1

  // keccak256 of 'DefaultPartition' obtained on https://emn178.github.io/online-tools/keccak_256.html
  const DEFAULT_PARTITION =
    "0x006c1bba7e26ac5fd3c1bccdafd91683870919a73b8c71fedfff33b9d1ebd6a2";
  const NAME = "Talium Token";
  const SYMBOL = "TT";
  const DECIMALS = 0;

  // Main ERC1400 contract deployment
  await deployer.deploy(
    ERC1400,
    NAME,
    SYMBOL,
    DECIMALS,
    DEFAULT_PARTITION,
    CONTROLLER
  );

  await deployer.deploy(
    TransactionValidator,
    ERC1400.address,
    CERTIFICATE_SIGNER
  );

  const deployedERC1400 = await ERC1400.deployed();
  const deployedTransactionValidator = await TransactionValidator.deployed();

  // Set validator on ERC1400
  await deployedERC1400.configure(TransactionValidator.address);

  // Transfer ownership of all contracts to the issuer
  await deployedERC1400.transferOwnership(OWNER);
  await deployedTransactionValidator.transferOwnership(OWNER);

  // print details
  console.log("CONTRACT DEPLOYED");
  console.log("| ERC1400          |", ERC1400.address);
  console.log("| Validator        |", TransactionValidator.address);
  console.log("| ERC1400 owner    |", await deployedERC1400.owner());
  console.log(
    "| Validator owner  |",
    await deployedTransactionValidator.owner()
  );
};
