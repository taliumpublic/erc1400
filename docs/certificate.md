# Certificate Format

The certificat verified by the contract has a different format depending on the
smart contract version.

All versions use the same general structure but the content of each part is
slightly different in each case.

![certificate-structure](./certificate-structure.png)

A certificate is emitted for three different kind of operations: a token
transfer, an issuance (token creation) or a redemption (token destruction).

 * The "common" part is the same, regardless of the the operation
 * The "operation-specific" part contains information specific to the kind of
   operation

Since the certificate must be decoded by the smart contract, Ethereum ABI
encoding is used to encode the structure.  Pseudo-code:

    bytes common = abi.encode(commonParameter1, commonParameter2, ...)
    bytes specific = abi.encode(specificParameter1, specificParameter2, ...)
    bytes certificate = abi.encode(common, specific)
    bytes signedCertificate = abi.encode(certificate, Sign(certificate))

## Current version (v3)

### Common parameters

The current version (3), uses the following parameters in `common`:

 * `version` (`uint8`), must be set to 3
 * `contractAddress` (`address`): the address of the ERC1400 smart contract this
   certificate was issued for,
 * `nonce` (`uint256`): unique nonce to prevent certificate reuse,
 * `validFrom` (`uint256`): date from which the certificate is valid,
 * `validTo` (`uint256`): date until which the certificate is valid,
 * `sender` (`address`): sender address,
 * `timestamp` (`uint64`): timestamp of the operation (this can be used to
   record an old operation in the blockchain),
 * `operation` (`uint8`): kind of operation (`TRANSFER=0`, `REDEMPTION=1`,
   `ISSUANCE=2`)

### Transfer operation parameters

If the `operation` field is `0` in the common part, the operation-specific
parameters are the following:

 * `from` (`address`): source address,
 * `to` (`address`): destionation address,
 * `partitionFrom` (`bytes32`): source partition,
 * `partitionTo` (`bytes32`): destination partition,
 * `amount`: (`uint256`): number of tokens to transfer.

### Redemption operation parameters

If the `operation` field is `1` in the common part, the operation-specific
parameters are the following:

 * `from` (`address`): source address,
 * `partitionFrom` (`bytes32`): source partition,
 * `amount`: (`uint256`): number of tokens to redeem.

### Issuance operation parameters

If the `operation` field is `2` in the common part, the operation-specific
parameters are the following:

 * `to` (`address`): destionation address,
 * `partitionTo` (`bytes32`): destination partition,
 * `amount`: (`uint256`): number of tokens to issue.

## Version 2

### Common parameters

The difference compared to version 3 is that there is no `version` and
`timestamp` fields in the common block.

## Version 1

There are some issues with the format used by version 1 and it is documented
but should not be used anymore.

 * Since operation code is stored in the operation-specific block, so it is not
   possible after decoding the common block to know what kind of operation the
   certificated is for (without additional information)
 * The sender address is not included

### Common parameters

The first version used the following layout for common parameters:

 * `contractAddress` (`address`): the address of the ERC1400 smart contract this
   certificate was issued for,
 * `nonce` (`uint256`): unique nonce to prevent certificate reuse,
 * `validFrom` (`uint256`): date from which the certificate is valid,
 * `validTo` (`uint256`): date until which the certificate is valid,

### Transfer operation parameters

 * `code` (`uint`): 0,
 * `from` (`address`): source address,
 * `to` (`address`): destionation address,
 * `partitionFrom` (`bytes32`): source partition,
 * `partitionTo` (`bytes32`): destination partition,
 * `amount`: (`uint256`): number of tokens to transfer.

### Redemption operation parameters

 * `code` (`uint`): 1,
 * `from` (`address`): source address,
 * `partitionFrom` (`bytes32`): source partition,
 * `amount`: (`uint256`): number of tokens to redeem.

### Issuance operation parameters

 * `code` (`uint`): 2,
 * `to` (`address`): destionation address,
 * `partitionTo` (`bytes32`): destination partition,
 * `amount`: (`uint256`): number of tokens to issue.

