import { partitions, signer, ZERO_ADDRESS } from "./helpers/common";
import { checkTxEvents, EventObjects } from "./helpers/event_asserts";
import Reverts from "./helpers/reverts";

const ERC1400 = artifacts.require("./StandaloneERC1400.sol");
const TransactionValidatorMock = artifacts.require(
  "./validation/TransactionValidatorMock.sol"
);

contract("ERC1643", (accounts) => {
  const account1 = accounts[1];
  const controller = accounts[7];
  const tokenOwner = accounts[9];
  const storageOwner = accounts[8];

  const emptyName = web3.utils.asciiToHex("");
  const uri = "https://www.gogl.bts.fly";
  const modifiedURI = "https://www.bts.l";
  const doc1Hash =
    "0x68656c6c6f000000000000000000000000000000000000000000000000000000";
  const doc2Hash =
    "0x667269656e640000000000000000000000000000000000000000000000000000";

  const doc1 =
    "0x0000000000000000000000000000000000000000000000000000000000000001";
  const doc2 =
    "0x0000000000000000000000000000000000000000000000000000000000000002";
  const docWithEmptyHash =
    "0x0000000000000000000000000000000000000000000000000000000000000003";
  const docNonExisting =
    "0x0000000000000000000000000000000000000000000000000000000000000042";

  const empty_hash =
    "0x0000000000000000000000000000000000000000000000000000000000000000";

  let erc1400;
  let contractsWithEvents;
  before(async () => {
    erc1400 = await ERC1400.new("", "", 0, partitions.default, controller, {
      from: tokenOwner,
    });

    const transactionValidator = await TransactionValidatorMock.new({
      from: tokenOwner,
    });

    erc1400.configure(transactionValidator.address, {
      from: tokenOwner,
    });

    contractsWithEvents = [erc1400];
  });

  describe(`setDocument()`, async () => {
    it("should fail when msg.sender is not the owner", async () => {
      await Reverts.assertNotOwner(
        erc1400.setDocument(doc1, uri, "0x0", {
          from: account1,
        })
      );
    });

    it("should revert when name is empty (setDocument)", async () => {
      await Reverts.assertEmptyDocName(
        erc1400.setDocument(emptyName, uri, "0x0", {
          from: tokenOwner,
        })
      );
    });

    it("should revert when name is empty (getDocument)", async () => {
      await Reverts.assertEmptyDocName(
        erc1400.getDocument(emptyName, {
          from: tokenOwner,
        })
      );
    });

    it("should revert when uri is empty", async () => {
      await Reverts.assertEmptyDocURI(
        erc1400.setDocument(doc1, "", "0x0", {
          from: tokenOwner,
        })
      );
    });

    it("should add new document", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.setDocument(doc1, uri, doc1Hash, {
          from: tokenOwner,
        }),
        EventObjects.documentUpdated(doc1, uri, doc1Hash)
      );

      assert.lengthOf(await erc1400.getAllDocuments(), 1);
      const doc = await erc1400.getDocument(doc1);
      assert.equal(doc[0], uri);
      assert.equal(doc[1], doc1Hash);
    });

    it("should add another document", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.setDocument(doc2, uri, doc2Hash, {
          from: tokenOwner,
        }),
        EventObjects.documentUpdated(doc2, uri, doc2Hash)
      );

      assert.lengthOf(await erc1400.getAllDocuments(), 2);

      const doc = await erc1400.getDocument(doc2);
      assert.equal(doc[0], uri);
      assert.equal(doc[1], doc2Hash);
    });

    it("should succeed when docHash is empty", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.setDocument(docWithEmptyHash, uri, empty_hash, {
          from: tokenOwner,
        }),
        EventObjects.documentUpdated(docWithEmptyHash, uri, empty_hash)
      );

      assert.lengthOf(await erc1400.getAllDocuments(), 3);

      const doc = await erc1400.getDocument(docWithEmptyHash);
      assert.equal(doc[0], uri);
      assert.equal(doc[1], empty_hash);
    });

    it("should update existing document", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.setDocument(doc2, modifiedURI, empty_hash, {
          from: tokenOwner,
        }),
        EventObjects.documentUpdated(doc2, modifiedURI, empty_hash)
      );

      assert.lengthOf(await erc1400.getAllDocuments(), 3);

      const doc = await erc1400.getDocument(doc2);
      assert.equal(doc[0], modifiedURI);
      assert.equal(doc[1], empty_hash);
    });
  });

  describe("removeDocument()", async () => {
    it("should fail to remove document when msg.sender is not owner", async () => {
      await Reverts.assertNotOwnerNorController(
        erc1400.removeDocument(doc2, { from: account1 })
      );
    });

    it("should revert when deleting a non-existing document", async () => {
      await Reverts.assertDocumentNotExists(
        erc1400.removeDocument(docNonExisting, {
          from: tokenOwner,
        })
      );
    });

    it("should remove document", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.removeDocument(doc1, {
          from: tokenOwner,
        }),
        EventObjects.documentRemoved(doc1, uri, doc1Hash)
      );
      assert.lengthOf(await erc1400.getAllDocuments(), 2);
    });

    it("should revert when trying to remove a removed document", async () => {
      await Reverts.assertDocumentNotExists(
        erc1400.removeDocument(doc1, { from: tokenOwner })
      );
    });

    it("should delete document with empty hash", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.removeDocument(docWithEmptyHash, {
          from: tokenOwner,
        }),
        EventObjects.documentRemoved(docWithEmptyHash, uri, empty_hash)
      );

      assert.lengthOf(await erc1400.getAllDocuments(), 1);
    });

    it("should delete an updated document", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.removeDocument(doc2, {
          from: tokenOwner,
        }),
        EventObjects.documentRemoved(doc2, modifiedURI, empty_hash)
      );
      assert.lengthOf(await erc1400.getAllDocuments(), 0);
    });
  });
});
