import Asserts from "./helpers/asserts";
import {
  action_codes,
  changePartitionData,
  createSignedCertificate,
  transferSignedCertificate,
  issueSignedCertificate,
  redeemSignedCertificate,
  generateCommonParams,
  generateFunctionParams,
  partitions,
  signer,
  ZERO_ADDRESS,
} from "./helpers/common";
import { checkTxEvents, EventObjects } from "./helpers/event_asserts";
import Reverts from "./helpers/reverts";

const ERC1400 = artifacts.require("./StandaloneERC1400.sol");
const TransactionValidator = artifacts.require(
  "./validation/TransactionValidator.sol"
);

contract("ERC1400", (accounts) => {
  const tokenHolder1 = accounts[3];
  const tokenHolder2 = accounts[2];
  const operator1 = accounts[5];
  const operator2 = accounts[6];
  const controller = accounts[8];
  const storageOwner = accounts[7];
  const tokenOwner = accounts[9];

  let erc1400;
  let transactionValidator;
  let contractAddress;
  let contractsWithEvents;
  before(async () => {
    erc1400 = await ERC1400.new("", "", 0, partitions.default, controller, {
      from: tokenOwner,
    });

    transactionValidator = await TransactionValidator.new(
      erc1400.address,
      signer,
      {
        from: tokenOwner,
      }
    );

    erc1400.configure(transactionValidator.address, {
      from: tokenOwner,
    });
    contractAddress = erc1400.address;
    contractsWithEvents = [erc1400];
  });

  describe("Global Operator", async () => {
    it("should authorize a global operator", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.authorizeOperator(operator1, {
          from: tokenHolder1,
        }),
        EventObjects.authorizedOperator(operator1, tokenHolder1)
      );

      assert.isTrue(await erc1400.isOperator(operator1, tokenHolder1));
    });

    it("should operatorByPartition return true if operator is global", async () => {
      assert.isTrue(
        await erc1400.isOperatorForPartition(
          partitions.one,
          operator1,
          tokenHolder1
        )
      );
    });

    it("should operatorTransferByPartition succeed from operator", async () => {
      const mintCertificate = issueSignedCertificate(
        contractAddress,
        tokenOwner,
        tokenHolder1,
        partitions.one,
        100
      );

      // issue tokens for testing purposes
      await erc1400.issueByPartition(
        partitions.one,
        tokenHolder1,
        100,
        mintCertificate,
        { from: tokenOwner }
      );

      // create certificate for transfer
      const certificate = transferSignedCertificate(
        contractAddress,
        operator1,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.one,
        10
      );

      // execute transaction
      await checkTxEvents(
        contractsWithEvents,
        erc1400.operatorTransferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          "0x0",
          { from: operator1 }
        ),
        EventObjects.transferByPartition(
          partitions.one,
          operator1,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          "0x00"
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, 10)
      );

      // check balances
      await Asserts.balanceOf(erc1400, tokenHolder1, 90);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        90
      );
      await Asserts.balanceOf(erc1400, tokenHolder2, 10);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder2,
        10
      );
    });

    it("should fail when sender is not the operator or the controller", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        operator2,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.one,
        2
      );

      await Reverts.assertUnauthorizedOperator(
        erc1400.operatorTransferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder2,
          2,
          certificate,
          "0x0",
          { from: operator2 }
        )
      );
    });

    it("should redeem", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        operator1,
        tokenHolder1,
        partitions.one,
        10
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.operatorRedeemByPartition(
          partitions.one,
          tokenHolder1,
          10,
          certificate,
          "0x0",
          { from: operator1 }
        ),
        EventObjects.redeemedByPartition(
          partitions.one,
          operator1,
          tokenHolder1,
          10,
          certificate,
          "0x00"
        ),
        EventObjects.redeemed(operator1, tokenHolder1, 10, certificate),
        EventObjects.transfer(tokenHolder1, ZERO_ADDRESS, 10)
      );

      await Asserts.balanceOf(erc1400, tokenHolder1, 80);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        80
      );
      await Asserts.totalSupply(erc1400, 90);
    });

    it("should allow transfer between partitions", async () => {
      // create certificate
      const certificate = transferSignedCertificate(
        contractAddress,
        operator1,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.two,
        10
      );

      // execute transaction
      await checkTxEvents(
        contractsWithEvents,
        erc1400.operatorTransferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          changePartitionData(partitions.two),
          { from: operator1 }
        ),
        EventObjects.transferByPartition(
          partitions.one,
          operator1,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          changePartitionData(partitions.two)
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, 10)
        //EventObjects.transferBetweenPartition(
        //  partitions.one,
        //  partitions.two,
        //  10
        //)
      );

      // check balances
      await Asserts.balanceOf(erc1400, tokenHolder1, 70);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        70
      );
      await Asserts.balanceOf(erc1400, tokenHolder2, 20);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.two,
        tokenHolder2,
        10
      );
      await Asserts.totalSupply(erc1400, 90);
    });

    it("should fail when transferBetweenPartition is given wrong data", async () => {
      // create certificate
      const certificate = transferSignedCertificate(
        contractAddress,
        operator1,
        tokenHolder1,
        tokenHolder2,
        partitions.two,
        partitions.one,
        10
      );

      // execute transaction
      await Reverts.assertInvalidCertificateParameters(
        erc1400.operatorTransferByPartition(
          partitions.one, // Should be from partition 2
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          "0x00",
          { from: operator1 }
        )
      );
    });

    it("should revoke the operator", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.revokeOperator(operator1, {
          from: tokenHolder1,
        }),
        EventObjects.revokedOperator(operator1, tokenHolder1)
      );

      assert.isFalse(await erc1400.isOperator(operator1, tokenHolder1));
    });

    it("should fail to transfer when revoked", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        operator1,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.two,
        2
      );

      await Reverts.assertUnauthorizedOperator(
        erc1400.operatorTransferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder2,
          2,
          certificate,
          "0x0",
          { from: operator1 }
        )
      );
    });

    it("should fail to redeem when revoked", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        operator1,
        tokenHolder1,
        partitions.one,
        20
      );

      await Reverts.assertUnauthorizedOperator(
        erc1400.operatorRedeemByPartition(
          partitions.one,
          tokenHolder1,
          20,
          certificate,
          "0x0",
          { from: operator1 }
        )
      );
    });
  });

  describe("OperatorForPartition", async () => {
    it("should succesfully authorize the operator by partition", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.authorizeOperatorByPartition(partitions.one, operator2, {
          from: tokenHolder1,
        }),
        EventObjects.authorizedOperatorByPartition(
          operator2,
          partitions.one,
          tokenHolder1
        )
      );

      assert.isTrue(
        await erc1400.isOperatorForPartition(
          partitions.one,
          operator2,
          tokenHolder1
        )
      );
    });

    it("should not be considered as a global operator", async () => {
      assert.isFalse(await erc1400.isOperator(operator2, tokenHolder1));
    });

    it("should transfer tokens", async () => {
      // create certificate
      const certificate = transferSignedCertificate(
        contractAddress,
        operator2,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.one,
        10
      );

      // execute transaction
      await checkTxEvents(
        contractsWithEvents,
        erc1400.operatorTransferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          "0x0",
          { from: operator2 }
        ),
        EventObjects.transferByPartition(
          partitions.one,
          operator2,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          "0x00"
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, 10)
      );

      await Asserts.balanceOf(erc1400, tokenHolder1, 60);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        60
      );
      await Asserts.balanceOf(erc1400, tokenHolder2, 30);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder2,
        20
      );
    });

    it("should fail to transfer tokens from another partition", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        operator2,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.two,
        2
      );

      await Reverts.assertUnauthorizedOperator(
        erc1400.operatorTransferByPartition(
          partitions.two,
          tokenHolder1,
          tokenHolder2,
          2,
          certificate,
          "0x0",
          { from: operator2 }
        )
      );
    });

    it("should redeem", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        operator2,
        tokenHolder1,
        partitions.one,
        10
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.operatorRedeemByPartition(
          partitions.one,
          tokenHolder1,
          10,
          certificate,
          "0x0",
          { from: operator2 }
        ),
        EventObjects.redeemedByPartition(
          partitions.one,
          operator2,
          tokenHolder1,
          10,
          certificate,
          "0x00"
        ),
        EventObjects.redeemed(operator2, tokenHolder1, 10, certificate, "0x00")
      );

      // check balances
      await Asserts.balanceOf(erc1400, tokenHolder1, 50);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        50
      );
      await Asserts.totalSupply(erc1400, 80);
    });

    it("should fail to redeem on another partition", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        operator2,
        tokenHolder1,
        partitions.default,
        20
      );

      await Reverts.assertUnauthorizedOperator(
        erc1400.operatorRedeemByPartition(
          partitions.default,
          tokenHolder1,
          20,
          certificate,
          "0x0",
          { from: operator2 }
        )
      );
    });

    it("should revoke succeed", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.revokeOperatorByPartition(partitions.one, operator2, {
          from: tokenHolder1,
        }),
        EventObjects.revokedOperatorByPartition(
          operator2,
          partitions.one,
          tokenHolder1
        )
      );

      assert.isFalse(
        await erc1400.isOperatorForPartition(
          partitions.one,
          operator2,
          tokenHolder1
        )
      );
    });

    it("should fail to transfer when revoked", async () => {
      // create certificate
      const certificate = transferSignedCertificate(
        contractAddress,
        operator2,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.one,
        2
      );

      await Reverts.assertUnauthorizedOperator(
        erc1400.operatorTransferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder2,
          2,
          certificate,
          "0x0",
          { from: operator2 }
        )
      );
    });

    it("should fail to redeem when revoked", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        operator2,
        tokenHolder1,
        partitions.one,
        20
      );

      await Reverts.assertUnauthorizedOperator(
        erc1400.operatorRedeemByPartition(
          partitions.one,
          tokenHolder1,
          20,
          certificate,
          "0x0",
          { from: operator2 }
        )
      );
    });
  });
});
