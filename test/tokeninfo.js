import { ZERO_ADDRESS, partitions, signer } from "./helpers/common";
import Reverts from "./helpers/reverts";

const ERC1400 = artifacts.require("./StandaloneERC1400.sol");
const TransactionValidator = artifacts.require(
  "./validation/TransactionValidator.sol"
);

contract("ERC1400", (accounts) => {
  const tokenHolder1 = accounts[3];
  const tokenOwner = accounts[9];
  const controller = accounts[8];

  const name = "Talium Token";
  const symbol = "TT";
  const decimals = 0;

  let erc1400Token;
  let transactionValidator;

  before(async () => {
    erc1400Token = await ERC1400.new(
      name,
      symbol,
      decimals,
      partitions.default,
      controller,
      {
        from: tokenOwner,
      }
    );

    transactionValidator = await TransactionValidator.new(
      erc1400Token.address,
      signer,
      {
        from: tokenOwner,
      }
    );

    erc1400Token.configure(transactionValidator.address, {
      from: tokenOwner,
    });
  });

  describe("Basic token information", async () => {
    it("Should have correct basic token information", async () => {
      assert.equal(await erc1400Token.name.call(), name);
      assert.equal(await erc1400Token.symbol.call(), symbol);
      assert.equal(await erc1400Token.decimals.call(), decimals);
    });

    it("Should be controllable", async () => {
      assert.isTrue(await erc1400Token.isControllable.call());
    });

    it("Should be issuable", async () => {
      assert.isTrue(await erc1400Token.isIssuable.call());
    });
  });

  describe("Default partition", async () => {
    it("should initialize with correct defaultPartition", async () => {
      assert.equal(
        partitions.default,
        await erc1400Token.getDefaultPartition()
      );
    });

    it("should fail to change default partition when caller is not the owner", async () => {
      await Reverts.assertNotOwner(
        erc1400Token.setDefaultPartition(partitions.one, {
          from: tokenHolder1,
        })
      );
    });

    it("should allow overriding the partition if the caller is the owner", async () => {
      erc1400Token.setDefaultPartition(partitions.one, {
        from: tokenOwner,
      });
      assert.equal(partitions.one, await erc1400Token.getDefaultPartition());
    });

    it("should succeed in reverting the default partition to the base default partition", async () => {
      erc1400Token.setDefaultPartition(partitions.default, {
        from: tokenOwner,
      }),
        assert.equal(
          partitions.default,
          await erc1400Token.getDefaultPartition()
        );
    });
  });

  describe("Validator contract information", async () => {
    it("should return validator address", async () => {
      assert.equal(
        transactionValidator.address,
        await erc1400Token.getTransactionValidator()
      );
    });
  });
});
