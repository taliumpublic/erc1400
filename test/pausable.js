import Asserts from "./helpers/asserts";
import {
  action_codes,
  createSignedCertificate,
  generateCommonParams,
  generateFunctionParams,
  transferSignedCertificate,
  issueSignedCertificate,
  redeemSignedCertificate,
  partitions,
  signer,
  ZERO_ADDRESS,
} from "./helpers/common";
import OpCodeAssertion from "./helpers/errorcodes";
import { checkTxEvents, EventObjects } from "./helpers/event_asserts";
import Reverts from "./helpers/reverts";

const TransactionValidator = artifacts.require(
  "./validation/TransactionValidator.sol"
);
const ERC1400 = artifacts.require("./StandaloneERC1400.sol");

contract("ERC1400/TransactionValidator", (accounts) => {
  const tokenHolder1 = accounts[3];
  const tokenHolder2 = accounts[2];
  const operator1 = accounts[5];
  const storageOwner = accounts[8];
  const tokenOwner = accounts[9];
  const controller = accounts[7];

  const empty_data = "0x0000000000000000000000000000000000000000";

  let transactionValidator;
  let erc1400;
  let contractAddress;
  let contractsWithEvents;

  before(async () => {
    erc1400 = await ERC1400.new("", "", 0, partitions.default, controller, {
      from: tokenOwner,
    });

    transactionValidator = await TransactionValidator.new(
      erc1400.address,
      signer,
      {
        from: tokenOwner,
      }
    );

    erc1400.configure(transactionValidator.address, {
      from: tokenOwner,
    });
    contractAddress = erc1400.address;
    contractsWithEvents = [erc1400];
  });

  describe("Pausable feature", async () => {
    it("should pause", async () => {
      await transactionValidator.pause({ from: tokenOwner });
      assert.isTrue(await transactionValidator.paused());
    });

    it("should return error code 0x42 in canTransferByPartition", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        tokenHolder1,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.one,
        1
      );

      const response = await erc1400.canTransferByPartition(
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        1,
        certificate,
        { from: tokenHolder1 }
      );
      OpCodeAssertion.tokenPausedCode(response);
    });

    it("should not transferByPartition when token is paused", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        tokenHolder1,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.one,
        1
      );

      await Reverts.assertPaused(
        erc1400.transferByPartition(
          partitions.one,
          tokenHolder2,
          1,
          certificate,
          { from: tokenHolder1 }
        )
      );
    });
    it("should not issueByPartition when token is paused", async () => {
      await Reverts.assertPaused(
        erc1400.issueByPartition(partitions.one, tokenHolder1, 10, "0x0", {
          from: tokenOwner,
        })
      );
    });

    it("should fail to issue tokens when token is paused", async () => {
      //await transactionValidator.pause({ from: tokenOwner });

      await Reverts.assertPaused(
        erc1400.issue(tokenHolder2, 100, empty_data, {
          from: tokenOwner,
        })
      );
    });

    it("should fail to transfer tokens when token is paused", async () => {
      await Reverts.assertPaused(
        erc1400.transferWithData(tokenHolder1, 100, empty_data, {
          from: tokenHolder2,
        })
      );
    });

    it("should fail to transfer tokens when token is paused (transferFrom)", async () => {
      await erc1400.increaseAllowance(operator1, 100, {
        from: tokenHolder1,
      });

      await Reverts.assertPaused(
        erc1400.transferFromWithData(
          tokenHolder1,
          tokenHolder2,
          100,
          empty_data,
          {
            from: operator1,
          }
        )
      );
    });

    it("should fail to redeem token because token is paused", async () => {
      await Reverts.assertPaused(
        erc1400.redeem(100, empty_data, {
          from: tokenHolder1,
        })
      );
    });

    it("should fail to redeem token because token is paused (redeemFrom)", async () => {
      await erc1400.authorizeOperator(operator1, { from: tokenHolder1 });
      await Reverts.assertPaused(
        erc1400.redeemFrom(tokenHolder1, 100, empty_data, {
          from: operator1,
        })
      );
    });

    it("should not modify isIssuable() when paused", async () => {
      assert.isTrue(await erc1400.isIssuable());
      await transactionValidator.unpause({ from: tokenOwner });
      assert.isTrue(await erc1400.isIssuable());
      await transactionValidator.pause({ from: tokenOwner });
    });

    it("should unpause", async () => {
      await transactionValidator.unpause({ from: tokenOwner });
      assert.isFalse(await transactionValidator.paused({ from: tokenOwner }));
    });
  });
});
