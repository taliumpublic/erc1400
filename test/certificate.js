import {
  action_codes,
  createSignedCertificate,
  encodeCertificate,
  encodeCommonParams,
  encodeFunctionParams,
  generateCommonParams,
  generateFunctionParams,
  now,
  partitions,
  signCertificate,
  signer,
  ZERO_ADDRESS,
} from "./helpers/common";
import Reverts from "./helpers/reverts";

const ERC1400 = artifacts.require("./StandaloneERC1400.sol");
const TransactionValidator = artifacts.require(
  "./validation/TransactionValidator.sol"
);

contract("TransactionValidator", (accounts) => {
  const tokenOwner = accounts[9];
  const allowedCaller = accounts[8];
  const addressFrom = accounts[0];
  const addressTo = accounts[1];
  const controller = accounts[7];
  const default_validFrom = undefined;
  // Untrusted signer address and key
  const unknownSigner = "0x8a29a067dfcd02d2feec23c734464fcff62e6e80";
  const unknownSignerPrivateKey =
    "0x957c5ff50b720a9fd5f5adedf9c520a2589ffea0219eb4ef4d78650c6efde63a";
  // Signer to be added as trusted by the unit tests
  const additionalSigner = "0x9233e7064ccea299dee195d5a1cb504e6a6b3839";
  const additionalSignerPrivateKey =
    "0x71e2f07340b6a7fbd46d87be36309a976d64ea7746229078be61c818cae2c818";

  const additionalController = "0x0a1cf2df4af24e0a67c5868ef28c0c4a8e5adeb5";

  let nonce = 0;

  let transactionValidator;
  let contractAddress;
  let erc1400;

  before(async () => {
    erc1400 = await ERC1400.new("", "", 0, partitions.default, controller, {
      from: tokenOwner,
    });

    transactionValidator = await TransactionValidator.new(
      erc1400.address,
      signer,
      {
        from: tokenOwner,
      }
    );

    contractAddress = transactionValidator.address;
  });

  describe("Deployment", async () => {
    it("should recognize trusted certificate signer passed in constructor", async () => {
      // Initial trusted signer
      assert.isTrue(await transactionValidator.isCertificateSigner(signer));

      // Untrusted signer
      assert.isFalse(
        await transactionValidator.isCertificateSigner(unknownSigner)
      );

      // Newly added signer
      assert.isFalse(
        await transactionValidator.isCertificateSigner(additionalSigner)
      );
    });

    it("should be issuable", async () => {
      assert.isTrue(await transactionValidator.isIssuable());
    });

    it("should set allowed caller", async () => {
      // Set the allowedCaller address as the allowed caller so that we can call
      // the validate/check function directly
      await transactionValidator.setAllowedCaller(allowedCaller, {
        from: tokenOwner,
      });
    });
  });

  describe("Signature verification", async () => {
    it("should extract correct signer address", async () => {
      const commonParams = generateCommonParams(
        contractAddress,
        tokenOwner, // Sender
        action_codes.TRANSFER_BY_PARTITION
      );
      const functionParams = generateFunctionParams(
        action_codes.TRANSFER_BY_PARTITION,
        addressFrom,
        addressTo,
        partitions.one,
        partitions.two,
        1
      );

      const unsignedCertificate = encodeCertificate(
        encodeCommonParams(commonParams),
        encodeFunctionParams(functionParams)
      );

      const certificateSignature = web3.eth.accounts.sign(
        web3.utils.soliditySha3(unsignedCertificate),
        unknownSignerPrivateKey
      ).signature;

      const result = await transactionValidator.extractSignerAddress.call(
        unsignedCertificate,
        certificateSignature
      );

      assert.equal(result.toLowerCase(), unknownSigner.toLowerCase());
    });

    it("should refuse certificate signed by unknown signer", async () => {
      const commonParams = generateCommonParams(
        contractAddress,
        tokenOwner, // Sender
        action_codes.TRANSFER_BY_PARTITION
      );
      const functionParams = generateFunctionParams(
        action_codes.TRANSFER_BY_PARTITION,
        addressFrom,
        addressTo,
        partitions.one,
        partitions.two,
        1
      );
      const certificate = createSignedCertificate(
        commonParams,
        functionParams,
        unknownSignerPrivateKey
      );

      const result = await transactionValidator.verifySignature.call(
        certificate
      );
      assert.equal(result.signerAddress, ZERO_ADDRESS);
    });

    it("should authenticate signed certificate", async () => {
      const commonParams = generateCommonParams(
        contractAddress,
        tokenOwner, // Sender
        action_codes.TRANSFER_BY_PARTITION
      );
      const functionParams = generateFunctionParams(
        action_codes.TRANSFER_BY_PARTITION,
        addressFrom,
        addressTo,
        partitions.one,
        partitions.two,
        1
      );
      const certificate = createSignedCertificate(commonParams, functionParams);

      const result = await transactionValidator.verifySignature.call(
        certificate
      );
      assert.isTrue(
        result.signerAddress.toLowerCase() === signer.toLowerCase()
      );
    });
  });

  describe("Administration", async () => {
    it("should add new trusted certificate signer", async () => {
      await transactionValidator.addCertificateSigner(additionalSigner, {
        from: tokenOwner,
      });

      // Initial trusted signer
      assert.isTrue(await transactionValidator.isCertificateSigner(signer));

      // Untrusted signer
      assert.isFalse(
        await transactionValidator.isCertificateSigner(unknownSigner)
      );

      // Newly added signer
      assert.isTrue(
        await transactionValidator.isCertificateSigner(additionalSigner)
      );
    });

    it("should revoke trusted certificate signer", async () => {
      assert.isTrue(
        await transactionValidator.isCertificateSigner(additionalSigner)
      );

      await transactionValidator.revokeCertificateSigner(additionalSigner, {
        from: tokenOwner,
      });

      assert.isFalse(
        await transactionValidator.isCertificateSigner(additionalSigner)
      );
    });

    it("should fail to add zero address as trusted certificate signer", async () => {
      Reverts.assertInvalidAddress(
        transactionValidator.addCertificateSigner(ZERO_ADDRESS, {
          from: tokenOwner,
        })
      );
    });
  });

  describe("Transaction validation", async () => {
    it("should accept a transfer by partition request", async () => {
      const commonParams = generateCommonParams(
        allowedCaller, // Contract address
        addressFrom, // Sender
        action_codes.TRANSFER_BY_PARTITION
      );
      const functionParams = generateFunctionParams(
        action_codes.TRANSFER_BY_PARTITION,
        addressFrom,
        addressTo,
        partitions.one,
        partitions.two,
        1
      );
      const certificate = createSignedCertificate(commonParams, functionParams);
      const encodedFunctionParams = encodeFunctionParams(functionParams);

      const result = await transactionValidator.checkTransfer(
        addressFrom, // Sender
        addressFrom,
        addressTo,
        partitions.one,
        1,
        certificate,
        {
          from: allowedCaller,
        }
      );

      assert.equal(result.status, "0x01");

      // After checking the certificate it should still be possible to validate
      // it (i.e. store the nonce and mark it "used")
      await transactionValidator.validateTransfer(
        addressFrom, // Sender of the transfer transaction
        addressFrom,
        addressTo,
        partitions.one,
        1,
        certificate,
        {
          from: allowedCaller,
        }
      );
    });

    it("should accept a redeem by partition request", async () => {
      const commonParams = generateCommonParams(
        allowedCaller,
        addressFrom, // Sender
        action_codes.REDEEM_BY_PARTITION
      );
      const functionParams = generateFunctionParams(
        action_codes.REDEEM_BY_PARTITION,
        addressFrom,
        null,
        partitions.one,
        null,
        1
      );
      const certificate = createSignedCertificate(commonParams, functionParams);
      const encodedFunctionParams = encodeFunctionParams(functionParams);

      await transactionValidator.validateRedemption(
        addressFrom,
        addressFrom,
        partitions.one,
        1,
        certificate,
        {
          from: allowedCaller,
        }
      );
    });

    it("should accept an issue by partition request", async () => {
      const commonParams = generateCommonParams(
        allowedCaller,
        tokenOwner,
        action_codes.ISSUE_BY_PARTITION
      );
      const functionParams = generateFunctionParams(
        action_codes.ISSUE_BY_PARTITION,
        null,
        addressTo,
        null,
        partitions.two,
        1
      );
      const certificate = createSignedCertificate(commonParams, functionParams);
      const encodedFunctionParams = encodeFunctionParams(functionParams);

      await transactionValidator.validateIssuance(
        tokenOwner,
        addressTo,
        partitions.two,
        1,
        certificate,
        {
          from: allowedCaller,
        }
      );
    });

    it("should fail if no certificate has been given", async () => {
      const functionParams = generateFunctionParams(
        action_codes.TRANSFER_BY_PARTITION,
        addressFrom,
        addressTo,
        partitions.one,
        partitions.two,
        1
      );
      const encodedFunctionParams = encodeFunctionParams(functionParams);

      const result = await transactionValidator.checkTransfer(
        addressFrom,
        addressFrom,
        addressTo,
        partitions.one,
        1,
        "0x0",
        { from: allowedCaller }
      );

      assert.equal("0x00", result.status);
    });

    it("should refuse the certificate if nonce already used", async () => {
      const nonceToReuse = 89485908045556;
      const commonParams = generateCommonParams(
        allowedCaller,
        addressFrom, // Sender
        action_codes.TRANSFER_BY_PARTITION,
        nonceToReuse
      );

      const functionParams = generateFunctionParams(
        action_codes.TRANSFER_BY_PARTITION, // transfer
        addressFrom,
        addressTo,
        partitions.one,
        partitions.two,
        1
      );
      const certificate = createSignedCertificate(commonParams, functionParams);
      const encodedFunctionParams = encodeFunctionParams(functionParams);

      // First time, validate the transaction and store the nonce
      const result1 = await transactionValidator.validateTransfer(
        addressFrom, // Sender
        addressFrom,
        addressTo,
        partitions.one,
        1,
        certificate,
        { from: allowedCaller }
      );

      // Try to reuse the nonce
      const result2 = await transactionValidator.checkTransfer(
        addressFrom, // Sender
        addressFrom,
        addressTo,
        partitions.one,
        1,
        certificate,
        { from: allowedCaller }
      );

      assert.equal(result2.status, "0x48");
    });

    it("should refuse the certificate if validFrom > now", async () => {
      const validFrom = now + 100;
      const commonParams = generateCommonParams(
        allowedCaller,
        addressFrom,
        action_codes.TRANSFER_BY_PARTITION,
        nonce,
        validFrom
      );
      const functionParams = generateFunctionParams(
        action_codes.TRANSFER_BY_PARTITION,
        addressFrom,
        addressTo,
        partitions.one,
        partitions.two,
        1
      );
      const certificate = createSignedCertificate(commonParams, functionParams);
      const encodedFunctionParams = encodeFunctionParams(functionParams);

      const result = await transactionValidator.checkTransfer(
        addressFrom,
        addressFrom,
        addressTo,
        partitions.one,
        1,
        certificate,
        { from: allowedCaller }
      );

      assert.equal("0x44", result.status);
    });

    it("should refuse the certificate if validTo < now", async () => {
      const validFrom = now;
      const validTo = now - 100;
      const commonParams = generateCommonParams(
        allowedCaller,
        addressFrom,
        action_codes.TRANSFER_BY_PARTITION,
        nonce,
        validFrom,
        validTo
      );
      const functionParams = generateFunctionParams(
        action_codes.TRANSFER_BY_PARTITION,
        addressFrom,
        addressTo,
        partitions.one,
        partitions.two,
        1
      );
      const certificate = createSignedCertificate(commonParams, functionParams);
      const encodedFunctionParams = encodeFunctionParams(functionParams);

      const result = await transactionValidator.checkTransfer(
        addressFrom,
        addressFrom,
        addressTo,
        partitions.one,
        1,
        certificate,
        { from: allowedCaller }
      );

      assert.equal(result.status, "0x46");
    });

    it("should refuse the certificate if signer is not registered", async () => {
      const unknown_signerPrivateKey =
        "0x354e5f3b3a8ff1c678e5a7223cc717ab3ca68a24cf4c36a949582e0852bd3c99";

      const commonParams = generateCommonParams(
        allowedCaller,
        addressFrom,
        action_codes.TRANSFER_BY_PARTITION
      );
      const functionParams = generateFunctionParams(
        action_codes.TRANSFER_BY_PARTITION,
        addressFrom,
        addressTo,
        partitions.one,
        partitions.two,
        1
      );
      const encodedCommonParams = encodeCommonParams(commonParams);
      const encodedFunctionParams = encodeFunctionParams(functionParams);
      const unsignedCertificate = encodeCertificate(
        encodedCommonParams,
        encodedFunctionParams
      );
      const certificate = signCertificate(
        unsignedCertificate,
        unknown_signerPrivateKey
      );

      const result = await transactionValidator.checkTransfer(
        addressFrom,
        addressFrom,
        addressTo,
        partitions.one,
        1,
        certificate,
        { from: allowedCaller }
      );

      assert.equal(result.status, "0xe0");
    });

    it("should refuse the certificate if contract address is wrong", async () => {
      const contractAddress = "0x772a4694ee9a83855267606e890b37cd9682685e";

      const commonParams = generateCommonParams(
        contractAddress,
        addressFrom,
        action_codes.TRANSFER_BY_PARTITION
      );
      const functionParams = generateFunctionParams(
        action_codes.TRANSFER_BY_PARTITION,
        addressFrom,
        addressTo,
        partitions.one,
        partitions.two,
        1
      );
      const certificate = createSignedCertificate(commonParams, functionParams);

      const encodedFunctionParams = encodeFunctionParams(functionParams);
      const result = await transactionValidator.checkTransfer(
        addressFrom,
        addressFrom,
        addressTo,
        partitions.one,
        1,
        certificate,
        { from: allowedCaller }
      );

      assert.equal(result.status, "0xe4");
    });
  });
});
