import Asserts from "./helpers/asserts";
import {
  action_codes,
  createSignedCertificate,
  transferSignedCertificate,
  issueSignedCertificate,
  redeemSignedCertificate,
  generateCommonParams,
  generateFunctionParams,
  partitions,
  signer,
  ZERO_ADDRESS,
  EMPTY_CERTIFICATE,
} from "./helpers/common";
import OpCodeAssertion from "./helpers/errorcodes";
import { checkTxEvents, EventObjects } from "./helpers/event_asserts";
import Reverts from "./helpers/reverts";

const ERC1400 = artifacts.require("./StandaloneERC1400.sol");
const TransactionValidatorMock = artifacts.require(
  "./validation/TransactionValidatorMock.sol"
);

contract("ERC1400", (accounts) => {
  const tokenHolder1 = accounts[3];
  const tokenHolder2 = accounts[2];
  const operator1 = accounts[5];
  const storageOwner = accounts[8];
  const tokenOwner = accounts[9];
  const controller = accounts[7];
  const allowanceAddress = accounts[6];

  let erc1400;
  let transactionValidator;
  let contractAddress;
  let contractsWithEvents;

  before(async () => {
    erc1400 = await ERC1400.new("", "", 0, partitions.default, controller, {
      from: tokenOwner,
    });

    transactionValidator = await TransactionValidatorMock.new({
      from: tokenOwner,
    });

    erc1400.configure(transactionValidator.address, {
      from: tokenOwner,
    });
    contractAddress = erc1400.address;
    contractsWithEvents = [erc1400];
  });

  describe("Issuance", async () => {
    it("should issue tokens on default partition", async () => {
      const amount = 100;

      const certificate = issueSignedCertificate(
        contractAddress,
        tokenOwner,
        tokenHolder1,
        partitions.default,
        amount
      );

      await checkTxEvents(
        [erc1400],
        erc1400.issue(tokenHolder1, amount, certificate, {
          from: tokenOwner,
        }),
        EventObjects.issuedByPartition(
          partitions.default,
          tokenHolder1,
          amount,
          certificate
        ),
        EventObjects.issued(tokenOwner, tokenHolder1, amount, certificate),
        EventObjects.transfer(ZERO_ADDRESS, tokenHolder1, amount)
      );

      // check total supply and balances
      await Asserts.totalSupply(erc1400, amount);
      await Asserts.balanceOf(erc1400, tokenHolder1, amount);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        amount
      );

      // check partitions of holder
      const tokenHolder1Partitions = await erc1400.partitionsOf(tokenHolder1);
      assert.lengthOf(tokenHolder1Partitions, 1);
      assert.equal(tokenHolder1Partitions[0], partitions.default);
    });
  });

  describe("ERC-20 Token Transfer", async () => {
    it("should transfer tokens from token holder 1 to token holder 2 ", async () => {
      const transferAmount = 5;

      await checkTxEvents(
        contractsWithEvents,
        erc1400.transfer(tokenHolder2, transferAmount, { from: tokenHolder1 }),
        EventObjects.transferByPartition(
          partitions.default,
          tokenHolder1,
          tokenHolder1,
          tokenHolder2,
          transferAmount,
          EMPTY_CERTIFICATE
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, transferAmount)
      );

      await Asserts.totalSupply(erc1400, 100);
      await Asserts.balanceOf(erc1400, tokenHolder1, 95);
      await Asserts.balanceOf(erc1400, tokenHolder2, 5);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        95
      );
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder2,
        5
      );
    });

    it("should transfer tokens from token holder 2 to token holder 1", async () => {
      const transferAmount = 1;
      const expectedHolder2Balance = 4;
      const expectedHolder1Balance = 96;

      await checkTxEvents(
        contractsWithEvents,
        erc1400.transfer(tokenHolder1, transferAmount, {
          from: tokenHolder2,
        }),
        EventObjects.transferByPartition(
          partitions.default,
          tokenHolder2,
          tokenHolder2,
          tokenHolder1,
          transferAmount,
          EMPTY_CERTIFICATE
        ),
        EventObjects.transfer(tokenHolder2, tokenHolder1, transferAmount)
      );

      // check balance for tokenHolder2
      await Asserts.balanceOf(erc1400, tokenHolder2, expectedHolder2Balance);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder2,
        expectedHolder2Balance
      );

      //check balance for tokenHolder1
      await Asserts.balanceOf(erc1400, tokenHolder1, expectedHolder1Balance);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        expectedHolder1Balance
      );
    });

    it("should fail to transfer when partition balance is insufficient", async () => {
      await Reverts.assertInsufficientBalance(
        erc1400.transfer(tokenHolder2, 150, { from: tokenHolder1 })
      );
    });

    it("should fail to transfer when receiver address is 0x", async () => {
      await Reverts.assertInvalidAddress(
        erc1400.transfer(ZERO_ADDRESS, 10, { from: tokenHolder1 })
      );
    });
  });

  describe("Allowances", async () => {
    it("transferFrom error if no approval", async () => {
      // "allowanceAddress" tries to transfers 2 tokens from TH1 to TH2
      Reverts.assertInsufficientAllowance(
        erc1400.transferFrom(tokenHolder1, tokenHolder2, 2, {
          from: allowanceAddress,
        })
      );
    });

    it("transferFrom success", async () => {
      // A certificate is necessary for that transfer to be accepted
      const certificate = transferSignedCertificate(
        contractAddress,
        allowanceAddress,
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        partitions.default,
        2
      );

      // Token holder 1 approves "allowanceAddress" for 10 tokens
      await checkTxEvents(
        [erc1400],
        erc1400.approve(allowanceAddress, 10, { from: tokenHolder1 }),
        EventObjects.approval(tokenHolder1, allowanceAddress, 10)
      );

      // "allowanceAddress" transfers 2 tokens from TH1 to TH2
      await checkTxEvents(
        contractsWithEvents,
        erc1400.transferFrom(tokenHolder1, tokenHolder2, 2, {
          from: allowanceAddress,
        }),
        EventObjects.transferByPartition(
          partitions.default,
          allowanceAddress,
          tokenHolder1,
          tokenHolder2,
          2,
          EMPTY_CERTIFICATE,
          null
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, 2),
        EventObjects.approval(tokenHolder1, allowanceAddress, 8)
      );

      // check totalSupply did not change
      await Asserts.totalSupply(erc1400, 100);
      // check tokenHolder1 balances
      await Asserts.balanceOf(erc1400, tokenHolder1, 94);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        94
      );
      // check tokenHolder2 balances
      await Asserts.balanceOf(erc1400, tokenHolder2, 6);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder2,
        6
      );
      // check allowance decrease too
      await Asserts.allowanceOf(erc1400, tokenHolder1, allowanceAddress, 8);

      // Reset TH1 allowance
      await erc1400.approve(allowanceAddress, 0, { from: tokenHolder1 });
    });
  });
});
