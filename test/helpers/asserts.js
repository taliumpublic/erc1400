const Asserts = {
  totalSupply: async (token, amount) => {
    assert.equal(web3.utils.fromWei(await token.totalSupply(), "wei"), amount);
  },

  balanceOf: async (token, holder, amount) => {
    assert.equal(
      web3.utils.fromWei(await token.balanceOf(holder), "wei"),
      amount
    );
  },

  balanceOfByPartition: async (token, partition, holder, amount) => {
    assert.equal(
      web3.utils.fromWei(
        await token.balanceOfByPartition(partition, holder),
        "wei"
      ),
      amount
    );
  },
  allowanceOf: async (token, holder, spender, amount) => {
    assert.equal(
      web3.utils.fromWei(await token.allowance(holder, spender), "wei"),
      amount
    );
  }
};

export default Asserts;
