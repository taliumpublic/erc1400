const checkTxEvents = async (contractWithEvents, fn, ...eventsData) => {
  const tx = await fn;
  const blockNumber = tx.receipt.blockNumber;

  let eventPromises = [];
  contractWithEvents.map((contract) =>
    eventPromises.push(
      contract.getPastEvents("allEvents", {
        fromBlock: blockNumber,
        toBlock: blockNumber,
      })
    )
  );

  let fullEventList = [];
  await Promise.all(eventPromises).then((res) =>
    res.forEach((eventList) =>
      eventList.forEach((ev) => fullEventList.push(ev))
    )
  );
  eventsData.forEach((evData, index) => {
    checkEventData(evData, fullEventList[index]);
  });
};

const checkEventData = (eventData, logData) => {
  assert.isOk(logData, "Event not emitted '" + eventData.name + "' as");
  assert.equal(logData.event, eventData.name, "Checking " + logData.event);
  delete eventData["name"];

  Object.entries(eventData).forEach(([key, val]) => {
    assert.equal(logData.args[key], val, "Checking " + logData.event);
  });
};

const EventObjects = {
  approval: (tokenHolder, spender, value) => {
    return {
      name: "Approval",
      owner: tokenHolder,
      spender: spender,
      value: value,
    };
  },
  authorizedOperator: (operator, tokenHolder) => {
    return {
      name: "AuthorizedOperator",
      _operator: operator,
      _tokenHolder: tokenHolder,
    };
  },
  authorizedOperatorByPartition: (operator, partition, tokenHolder) => {
    return {
      name: "AuthorizedOperatorByPartition",
      _operator: operator,
      _partition: partition,
      _tokenHolder: tokenHolder,
    };
  },
  controllerRedemption: (
    controller,
    tokenHolder,
    value,
    certificate,
    operatorData = null
  ) => {
    return {
      name: "ControllerRedemption",
      _controller: controller,
      _tokenHolder: tokenHolder,
      _value: value,
      _data: certificate,
      _operatorData: operatorData,
    };
  },
  controllerTransfer: (
    controller,
    from,
    to,
    value,
    certificate,
    operatorData = null
  ) => {
    return {
      name: "ControllerTransfer",
      _controller: controller,
      _from: from,
      _to: to,
      _value: value,
      _data: certificate,
      _operatorData: operatorData,
    };
  },
  defaultPartitionChanged: (newPartition) => {
    return {
      name: "DefaultPartitionChanged",
      _newDefaultPartition: newPartition,
    };
  },
  documentRemoved: (docName, docURI, docHash) => {
    return {
      name: "DocumentRemoved",
      _name: docName,
      _uri: docURI,
      _documentHash: docHash,
    };
  },
  documentUpdated: (docName, docURI, docHash) => {
    return {
      name: "DocumentUpdated",
      _name: docName,
      _uri: docURI,
      _documentHash: docHash,
    };
  },
  issued: (operator, tokenHolder, value, certificate) => {
    return {
      name: "Issued",
      _operator: operator,
      _to: tokenHolder,
      _value: value,
      _data: certificate,
    };
  },
  issuedByPartition: (partition, tokenHolder, value, certificate) => {
    return {
      name: "IssuedByPartition",
      _partition: partition,
      _to: tokenHolder,
      _value: value,
      _data: certificate,
    };
  },
  redeemed: (operator, tokenHolder, value, certificate) => {
    return {
      name: "Redeemed",
      _operator: operator,
      _from: tokenHolder,
      _value: value,
      _data: certificate,
    };
  },
  redeemedByPartition: (
    partition,
    operator,
    tokenHolder,
    amount,
    certificate,
    operatorData = null
  ) => {
    return {
      name: "RedeemedByPartition",
      _partition: partition,
      _operator: operator,
      _from: tokenHolder,
      _value: amount,
      _data: certificate,
      _operatorData: operatorData,
    };
  },
  revokedOperator: (operator, tokenHolder) => {
    return {
      name: "RevokedOperator",
      _operator: operator,
      _tokenHolder: tokenHolder,
    };
  },

  revokedOperatorByPartition: (operator, partition, tokenHolder) => {
    return {
      name: "RevokedOperatorByPartition",
      _operator: operator,
      _partition: partition,
      _tokenHolder: tokenHolder,
    };
  },
  transfer: (from, to, value) => {
    return {
      name: "Transfer",
      from: from,
      to: to,
      value: value,
    };
  },
  transferBetweenPartition: (fromPartition, toPartition, value) => {
    return {
      name: "TransferBetweenPartition",
      _fromPartition: fromPartition,
      _toPartition: toPartition,
      _value: value,
    };
  },
  transferByPartition: (
    fromPartition,
    operator,
    from,
    to,
    value,
    certificate,
    operatorData = null
  ) => {
    return {
      name: "TransferByPartition",
      _fromPartition: fromPartition,
      _operator: operator,
      _from: from,
      _to: to,
      _value: value,
      _data: certificate,
      _operatorData: operatorData,
    };
  },
};

export { checkTxEvents, EventObjects };
