// helper function that checks the content of the response
// response is like [ <errorCode>, <msg>, <additionnalMsg> ]
// check EIP1066
const assertErrorWithOpCode = (
  response,
  expectedErrorCode,
  expectedMsg,
  expectedSecondMsg = ""
) => {
  assert.equal(response[0], expectedErrorCode);
  assert.equal(web3.utils.toUtf8(response[1]), expectedMsg);
  if (
    response[2] ==
    "0x0000000000000000000000000000000000000000000000000000000000000000"
  ) {
    response[2] = "";
  }
  assert.equal(response[2], expectedSecondMsg);
};

// The code used are indicated from the EIP1066 standard and should be the same as in the Smart Contracts
// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-1066.md
const OpCodeAssertion = {
  tokenPausedCode: (response) => {
    assertErrorWithOpCode(response, 0x42, "");
  },
  transferCanSucceed: (response, partition) => {
    assertErrorWithOpCode(response, 0x03, "Success", partition);
  },
  invalidPartition: (response) => {
    assertErrorWithOpCode(response, 0x20, "Partition does not exist");
  },
  insufficientBalance: (response) => {
    assertErrorWithOpCode(response, 0x54, "");
  },
  invalidReceiver: (response) => {
    assertErrorWithOpCode(response, 0x10, "Invalid receiver");
  },
  invalidCertificate: (response) => {
    assertErrorWithOpCode(response, 0x10, "Certificate is not valid");
  },
};

export default OpCodeAssertion;
