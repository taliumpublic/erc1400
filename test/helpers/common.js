const signer = "0x772a4694ee9a83855267606e890b37cd9682685e";
const signerPrivateKey =
  "0xe41dbf5138f413c97b7d901ea4bb8e92703067b5bf9759a008ba6d18745cf48e";

const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";
const changePartitionFlag =
  "0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff";
// partitions

const action_codes = {
  TRANSFER_BY_PARTITION: 0,
  REDEEM_BY_PARTITION: 1,
  ISSUE_BY_PARTITION: 2,
};

const partitions = {
  one: "0x5265736572766564000000000000000000000000000000000000000000000000",
  two: "0x4973737565640000000000000000000000000000000000000000000000000000",
  three: "0x4c6f636b65640000000000000000000000000000000000000000000000000000",
  default: "0x006c1bba7e26ac5fd3c1bccdafd91683870919a73b8c71fedfff33b9d1ebd6a2",
};

const ether = (amount) => {
  return web3.utils.toWei(amount.toString());
};

const changePartitionData = (partition) => {
  return changePartitionFlag + partition.substr(2);
};

let defaultNonce = 0;
const now = Math.round(new Date().getTime() / 1000) - 60;
const defaultValidFrom = now;
const defaultValidTo = defaultValidFrom + 10000;

function generateCommonParams(
  _contractAddress,
  _sender,
  _functionCode,
  _nonce = defaultNonce,
  _validFrom = defaultValidFrom,
  _validTo = defaultValidTo
) {
  let commonParams = new Map();
  commonParams.contractAddress = _contractAddress;
  commonParams.nonce = _nonce;
  commonParams.validFrom = _validFrom;
  commonParams.validTo = _validTo;
  commonParams.sender = _sender;
  commonParams.timestamp = 0;
  commonParams.functionCode = _functionCode;

  defaultNonce = _nonce == defaultNonce ? (defaultNonce += 1) : defaultNonce;

  return commonParams;
}

function generateFunctionParams(
  _functionCode,
  _addressFrom,
  _addressTo,
  _partitionFrom,
  _partitionTo,
  _value
) {
  let functionParams = {};
  functionParams.functionCode = _functionCode;
  functionParams.addressFrom = _addressFrom;
  functionParams.addressTo = _addressTo;
  functionParams.partitionFrom = _partitionFrom;
  functionParams.partitionTo = _partitionTo;
  functionParams.value = _value;

  return functionParams;
}

function createSignedCertificate(
  _commonParams,
  _functionParams,
  _signerPrivateKey = signerPrivateKey
) {
  const encodedCommonParams = encodeCommonParams(_commonParams);
  const encodedFunctionParams = encodeFunctionParams(_functionParams);
  const certificate = encodeCertificate(
    encodedCommonParams,
    encodedFunctionParams
  );

  return signCertificate(certificate, _signerPrivateKey);
}

function signCertificate(_certificate, _signerPrivateKey = signerPrivateKey) {
  const hash = web3.utils.soliditySha3(_certificate);
  const signature = web3.eth.accounts.sign(hash, _signerPrivateKey);

  const signedCertificate = web3.eth.abi.encodeParameters(
    ["bytes", "bytes"],
    [_certificate, signature.signature]
  );
  return signedCertificate;
}

function encodeCertificate(_commonParamsEncoded, _functionParamsEncoded) {
  let certificate = web3.eth.abi.encodeParameters(
    ["bytes", "bytes"],
    [_commonParamsEncoded, _functionParamsEncoded]
  );

  return certificate;
}

function encodeCommonParams(params) {
  const encodedParams = web3.eth.abi.encodeParameters(
    ["uint8", "address", "uint256", "uint256", "uint256", "address", "uint64", "uint8"],
    [
      3, // Version
      params.contractAddress,
      params.nonce,
      params.validFrom,
      params.validTo,
      params.sender,
      params.timestamp,
      params.functionCode,
    ]
  );

  return encodedParams;
}

function encodeFunctionParams(_params) {
  let encodedParams;
  switch (_params.functionCode) {
    case action_codes.TRANSFER_BY_PARTITION:
      encodedParams = web3.eth.abi.encodeParameters(
        ["address", "address", "bytes32", "bytes32", "uint256"],
        [
          _params.addressFrom,
          _params.addressTo,
          _params.partitionFrom,
          _params.partitionTo,
          _params.value,
        ]
      );
      break;
    case action_codes.REDEEM_BY_PARTITION:
      encodedParams = web3.eth.abi.encodeParameters(
        ["address", "bytes32", "uint256"],
        [_params.addressFrom, _params.partitionFrom, _params.value]
      );
      break;
    case action_codes.ISSUE_BY_PARTITION:
      encodedParams = web3.eth.abi.encodeParameters(
        ["address", "bytes32", "uint256"],
        [_params.addressTo, _params.partitionTo, _params.value]
      );
      break;
    default:
      console.error(
        "ERC1400 function code " + _params.functionCode + " not recognized"
      );
  }

  return encodedParams;
}

function transferSignedCertificate(
  contractAddress,
  senderAddress,
  from,
  to,
  partitionFrom,
  partitionTo,
  amount
) {
  const commonParams = generateCommonParams(
    contractAddress,
    senderAddress,
    action_codes.TRANSFER_BY_PARTITION
  );
  const functionParams = generateFunctionParams(
    action_codes.TRANSFER_BY_PARTITION,
    from,
    to,
    partitionFrom,
    partitionTo,
    amount
  );
  return createSignedCertificate(commonParams, functionParams);
}

function issueSignedCertificate(
  contractAddress,
  senderAddress,
  to,
  partitionTo,
  amount
) {
  const commonParams = generateCommonParams(
    contractAddress,
    senderAddress,
    action_codes.ISSUE_BY_PARTITION
  );
  const functionParams = generateFunctionParams(
    action_codes.ISSUE_BY_PARTITION,
    null,
    to,
    null,
    partitionTo,
    amount
  );
  return createSignedCertificate(commonParams, functionParams);
}

function redeemSignedCertificate(
  contractAddress,
  senderAddress,
  from,
  partitionFrom,
  amount
) {
  const commonParams = generateCommonParams(
    contractAddress,
    senderAddress,
    action_codes.REDEEM_BY_PARTITION
  );
  const functionParams = generateFunctionParams(
    action_codes.REDEEM_BY_PARTITION,
    from,
    null,
    partitionFrom,
    null,
    amount
  );
  return createSignedCertificate(commonParams, functionParams);
}

export {
  action_codes,
  changePartitionData,
  encodeCertificate,
  encodeCommonParams,
  generateCommonParams,
  generateFunctionParams,
  createSignedCertificate,
  encodeFunctionParams,
  ether,
  now,
  partitions,
  signer,
  signCertificate,
  signerPrivateKey,
  ZERO_ADDRESS,
  transferSignedCertificate,
  issueSignedCertificate,
  redeemSignedCertificate,
};
