// This file should regroup all asserts on the modifiers
// if some Solidity contract has a modifier, it hsould be defined so as to test easily if it is called with the correct msg

// Helper function from the parity-contracts/kovan-validator-set testing
const assertThrowsAsync = async (fn, msg) => {
  try {
    await fn();
  } catch (err) {
    assert(
      err.message.includes(msg),
      "Expected error to include: " + msg + " got: " + err.message
    );
    return;
  }
  assert.fail("Expected fn to throw");
};

const assertCheckThrowsAsync = async (fn, msgcheckf) => {
  try {
    await fn();
  } catch (err) {
    assert(
      msgcheckf(err.message),
      `Unexpected error message : '${err.message}'`
    );
    return;
  }
  assert.fail("Expected fn to throw");
};

const Reverts = {
  assertPaused: async (fn) => {
    await assertCheckThrowsAsync(
      () => fn,
      (msg) => msg.includes("42") || msg.includes("Pausable")
    );
  },
  assertTokenNotFrozen: async (fn) => {
    await assertThrowsAsync(() => fn, "Token is not frozen");
  },
  assertNotOwner: async (fn) => {
    await assertCheckThrowsAsync(
      () => fn,
      (msg) =>
        msg.includes("Ownable: caller is not the owner") || msg.includes("10")
    );
  },
  assertWrongPartition: async (fn) => {
    await assertThrowsAsync(() => fn, "Invalid partition");
  },
  assertInvalidAddress: async (fn) => {
    await assertThrowsAsync(() => fn, "20");
  },
  assertPositiveNumberOfTokens: async (fn) => {
    await assertThrowsAsync(() => fn, "20");
  },
  assertInsufficientBalance: async (fn) => {
    await assertThrowsAsync(() => fn, "54");
  },
  assertInsufficientAllowance: async (fn) => {
    await assertThrowsAsync(() => fn, "56");
  },
  assertInvalidCertificateParameters: async (fn) => {
    await assertThrowsAsync(() => fn, "08");
  },
  assertInvalidCertificate: async (fn) => {
    await assertThrowsAsync(() => fn, "e4");
  },
  assertEmptyCertificate: async (fn) => {
    await assertThrowsAsync(() => fn, "00");
  },
  assertNotImplemented: async (fn) => {
    await assertThrowsAsync(() => fn, "NOT IMPLEMENTED");
  },
  assertUnauthorizedOperator: async (fn) => {
    await assertThrowsAsync(() => fn, "10");
  },
  assertControllerCanNotBeMsgSender: async (fn) => {
    await assertThrowsAsync(() => fn, "controller can not be issuer");
  },
  assertControllerCanNotBeZeroAddress: async (fn) => {
    await assertThrowsAsync(() => fn, "20");
  },
  assertNotController: async (fn) => {
    await assertThrowsAsync(() => fn, "10");
  },
  assertControllerFinalized: async (fn) => {
    await assertThrowsAsync(() => fn, "10");
  },
  assertNotOwnerNorController: async (fn) => {
    await assertThrowsAsync(() => fn, "10");
  },
  assertEmptyDocName: async (fn) => {
    await assertThrowsAsync(() => fn, "20");
  },
  assertEmptyDocURI: async (fn) => {
    await assertThrowsAsync(() => fn, "20");
  },
  assertDocumentNotExists: async (fn) => {
    await assertThrowsAsync(() => fn, "20");
  },
  assertSubstractOverflow: async (fn) => {
    await assertThrowsAsync(() => fn, "24");
  },
  assertOverflow: async (fn) => {
    await assertThrowsAsync(() => fn, "26");
  },
};

export default Reverts;
