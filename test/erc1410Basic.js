import Asserts from "./helpers/asserts";
import {
  action_codes,
  changePartitionData,
  createSignedCertificate,
  transferSignedCertificate,
  issueSignedCertificate,
  redeemSignedCertificate,
  generateCommonParams,
  generateFunctionParams,
  partitions,
  signer,
  ZERO_ADDRESS,
  EMPTY_CERTIFICATE,
} from "./helpers/common";
import OpCodeAssertion from "./helpers/errorcodes";
import { checkTxEvents, EventObjects } from "./helpers/event_asserts";
import Reverts from "./helpers/reverts";

const ERC1400 = artifacts.require("./StandaloneERC1400.sol");
const TransactionValidator = artifacts.require(
  "./validation/TransactionValidator.sol"
);

contract("ERC1400", (accounts) => {
  const tokenHolder1 = accounts[3];
  const tokenHolder2 = accounts[2];
  const operator1 = accounts[5];
  const storageOwner = accounts[8];
  const tokenOwner = accounts[9];
  const controller = accounts[7];

  let erc1400;
  let transactionValidator;
  let contractAddress;
  let contractsWithEvents;

  before(async () => {
    erc1400 = await ERC1400.new("", "", 0, partitions.default, controller, {
      from: tokenOwner,
    });

    transactionValidator = await TransactionValidator.new(
      erc1400.address,
      signer,
      {
        from: tokenOwner,
      }
    );

    erc1400.configure(transactionValidator.address, {
      from: tokenOwner,
    });
    contractAddress = erc1400.address;
    contractsWithEvents = [erc1400];
  });

  describe("Issuance", async () => {
    it("should issue tokens by partition", async () => {
      const amount = 10;

      const certificate = issueSignedCertificate(
        contractAddress,
        tokenOwner,
        tokenHolder1,
        partitions.one,
        amount
      );

      await checkTxEvents(
        [erc1400],
        erc1400.issueByPartition(
          partitions.one,
          tokenHolder1,
          amount,
          certificate,
          { from: tokenOwner }
        ),
        EventObjects.issuedByPartition(
          partitions.one,
          tokenHolder1,
          amount,
          certificate
        ),
        EventObjects.issued(tokenOwner, tokenHolder1, amount, certificate),
        EventObjects.transfer(ZERO_ADDRESS, tokenHolder1, amount)
      );

      // check total supply and balances
      await Asserts.totalSupply(erc1400, amount);
      await Asserts.balanceOf(erc1400, tokenHolder1, amount);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        amount
      );

      // check partitions of holder
      const tokenHolder1Partitions = await erc1400.partitionsOf(tokenHolder1);
      assert.lengthOf(tokenHolder1Partitions, 1);
      assert.equal(tokenHolder1Partitions[0], partitions.one);
    });

    it("should increase total supply and balance correctly when reissuing on the same tokenHolder on the same partition", async () => {
      const amountAdded = 20;
      const expectedTotalSupply = 30;

      const certificate = issueSignedCertificate(
        contractAddress,
        tokenOwner,
        tokenHolder1,
        partitions.one,
        amountAdded
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.issueByPartition(
          partitions.one,
          tokenHolder1,
          amountAdded,
          certificate,
          { from: tokenOwner }
        ),
        EventObjects.issuedByPartition(
          partitions.one,
          tokenHolder1,
          amountAdded,
          certificate
        ),
        EventObjects.issued(tokenOwner, tokenHolder1, amountAdded, certificate),
        EventObjects.transfer(ZERO_ADDRESS, tokenHolder1, amountAdded)
      );

      await Asserts.totalSupply(erc1400, expectedTotalSupply);
      await Asserts.balanceOf(erc1400, tokenHolder1, expectedTotalSupply);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        expectedTotalSupply
      );

      const tokenHolder1Partitions = await erc1400.partitionsOf(tokenHolder1);
      assert.lengthOf(tokenHolder1Partitions, 1);
      assert.equal(tokenHolder1Partitions[0], partitions.one);
    });

    it("should issue on multiple token holders without interference", async () => {
      const amountAdded = 50;
      const expectedTotalSupply = 80;
      const expectedAccount1Balance = 30;

      const certificate = issueSignedCertificate(
        contractAddress,
        tokenOwner,
        tokenHolder2,
        partitions.one,
        amountAdded
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.issueByPartition(
          partitions.one,
          tokenHolder2,
          amountAdded,
          certificate,
          { from: tokenOwner }
        ),
        EventObjects.issuedByPartition(
          partitions.one,
          tokenHolder2,
          amountAdded,
          certificate
        ),
        EventObjects.issued(tokenOwner, tokenHolder2, amountAdded, certificate),
        EventObjects.transfer(ZERO_ADDRESS, tokenHolder2, amountAdded)
      );

      // total supply increased check
      await Asserts.totalSupply(erc1400, expectedTotalSupply);

      // tokenHolder1 unaffected check
      await Asserts.balanceOf(erc1400, tokenHolder1, expectedAccount1Balance);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        expectedAccount1Balance
      );

      // tokenHolder2 with correct balance check
      await Asserts.balanceOf(erc1400, tokenHolder2, amountAdded);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder2,
        amountAdded
      );

      // check that number of partitions did not change for tokenHolder1
      const tokenHolder1Partitions = await erc1400.partitionsOf(tokenHolder1);

      assert.lengthOf(tokenHolder1Partitions, 1);
      assert.equal(tokenHolder1Partitions[0], partitions.one);

      // check that tokenHolder2 has one partition which is the correct one
      const tokenHolder2Partitions = await erc1400.partitionsOf(tokenHolder2);
      assert.lengthOf(tokenHolder2Partitions, 1);
      assert.equal(tokenHolder2Partitions[0], partitions.one);
    });

    it("should prevent issuing on the ZERO_ADDRESS", async () => {
      await Reverts.assertInvalidAddress(
        erc1400.issueByPartition(
          partitions.one,
          ZERO_ADDRESS,
          10,
          issueSignedCertificate(
            contractAddress,
            tokenOwner,
            ZERO_ADDRESS,
            partitions.one,
            10
          ),
          { from: tokenOwner }
        )
      );
    });

    it("should prevent issuing 0 tokens", async () => {
      await Reverts.assertPositiveNumberOfTokens(
        erc1400.issueByPartition(
          partitions.one,
          tokenHolder2,
          0,
          issueSignedCertificate(
            contractAddress,
            tokenOwner,
            tokenHolder2,
            partitions.one,
            0
          ),
          {
            from: tokenOwner,
          }
        )
      );
    });
  });

  describe("Token transfer", async () => {
    it("should transfer tokens from token holder 1 to token holder 2 ", async () => {
      const transferAmount = 5;

      const certificate = transferSignedCertificate(
        contractAddress,
        tokenHolder1,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.one,
        transferAmount
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.transferByPartition(
          partitions.one,
          tokenHolder2,
          transferAmount,
          certificate,
          { from: tokenHolder1 }
        ),
        EventObjects.transferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder1,
          tokenHolder2,
          transferAmount,
          certificate
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, transferAmount)
      );

      await Asserts.totalSupply(erc1400, 80);
      await Asserts.balanceOf(erc1400, tokenHolder1, 25);
      await Asserts.balanceOf(erc1400, tokenHolder2, 55);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        25
      );
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder2,
        55
      );
    });

    it("should transfer tokens from token holder 2 to token holder 1", async () => {
      const transferAmount = 10;
      const expectedHolder2Balance = 45;
      const expectedHolder1Balance = 35;

      const certificate = transferSignedCertificate(
        contractAddress,
        tokenHolder2,
        tokenHolder2,
        tokenHolder1,
        partitions.one,
        partitions.one,
        transferAmount
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.transferByPartition(
          partitions.one,
          tokenHolder1,
          transferAmount,
          certificate,
          { from: tokenHolder2 }
        ),
        EventObjects.transferByPartition(
          partitions.one,
          tokenHolder2,
          tokenHolder2,
          tokenHolder1,
          transferAmount,
          certificate
        ),
        EventObjects.transfer(tokenHolder2, tokenHolder1, transferAmount)
      );

      // check balance for tokenHolder2
      await Asserts.balanceOf(erc1400, tokenHolder2, expectedHolder2Balance);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder2,
        expectedHolder2Balance
      );

      //check balance for tokenHolder1
      await Asserts.balanceOf(erc1400, tokenHolder1, expectedHolder1Balance);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        expectedHolder1Balance
      );
    });

    it("should fail to transfer tokens from an inexistant partition", async () => {
      await Reverts.assertInsufficientBalance(
        erc1400.transferByPartition(
          partitions.two,
          tokenHolder2,
          40,
          transferSignedCertificate(
            contractAddress,
            tokenHolder1,
            tokenHolder1,
            tokenHolder2,
            partitions.two,
            partitions.two,
            40
          ),
          { from: tokenHolder1 }
        )
      );
    });

    it("should fail to transfer when partition balance is insufficient", async () => {
      await Reverts.assertInsufficientBalance(
        erc1400.transferByPartition(
          partitions.one,
          tokenHolder2,
          50,
          transferSignedCertificate(
            contractAddress,
            tokenHolder1,
            tokenHolder1,
            tokenHolder2,
            partitions.one,
            partitions.one,
            50
          ),
          { from: tokenHolder1 }
        )
      );
    });

    it("should fail to transfer when receiver address is 0x", async () => {
      await Reverts.assertInvalidAddress(
        erc1400.transferByPartition(
          partitions.one,
          ZERO_ADDRESS,
          10,
          transferSignedCertificate(
            contractAddress,
            tokenHolder1,
            tokenHolder1,
            ZERO_ADDRESS,
            partitions.one,
            partitions.one,
            10
          ),
          { from: tokenHolder1 }
        )
      );
    });

    it("should return balance of 0 when checking on a partition the holder does not have", async () => {
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.three,
        tokenHolder1,
        0
      );
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.two,
        tokenHolder2,
        0
      );
    });

    it("should allow transfering tokens on a partition the receiver does not have", async () => {
      const issueCertificate = issueSignedCertificate(
        contractAddress,
        tokenOwner,
        tokenHolder1,
        partitions.two,
        50
      );

      await erc1400.issueByPartition(
        partitions.two,
        tokenHolder1,
        50,
        issueCertificate,
        {
          from: tokenOwner,
        }
      );

      const amountTransfered = 15;
      const expectedTotalSupply = 130;
      const expectedTokenHolder1BalanceAfterTransfer = 70;
      const expectedTokenHolder1BalanceOnPartition2AfterTransfer = 35;
      const expectedTokenHolder2BalanceAfterTransfer = 60;
      const expectedTokenHolder2BalanceOnPartition2AfterTransfer = 15;

      const transferCertificate = transferSignedCertificate(
        contractAddress,
        tokenHolder1,
        tokenHolder1,
        tokenHolder2,
        partitions.two,
        partitions.two,
        amountTransfered
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.transferByPartition(
          partitions.two,
          tokenHolder2,
          amountTransfered,
          transferCertificate,
          { from: tokenHolder1 }
        ),
        EventObjects.transferByPartition(
          partitions.two,
          tokenHolder1,
          tokenHolder1,
          tokenHolder2,
          amountTransfered,
          transferCertificate
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, amountTransfered)
      );

      // check totalSupply did not change
      await Asserts.totalSupply(erc1400, expectedTotalSupply);

      // check tokenHolder1 balances
      await Asserts.balanceOf(
        erc1400,
        tokenHolder1,
        expectedTokenHolder1BalanceAfterTransfer
      );
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.two,
        tokenHolder1,
        expectedTokenHolder1BalanceOnPartition2AfterTransfer
      );

      // check tokenHolder2 balances
      await Asserts.balanceOf(
        erc1400,
        tokenHolder2,
        expectedTokenHolder2BalanceAfterTransfer
      );
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.two,
        tokenHolder2,
        expectedTokenHolder2BalanceOnPartition2AfterTransfer
      );

      // check that the partitions changed accordingly
      const holder1partitions = await erc1400.partitionsOf(tokenHolder1);
      const holder2partitions = await erc1400.partitionsOf(tokenHolder2);

      assert.lengthOf(holder1partitions, 2);
      assert.lengthOf(holder2partitions, 2);
      assert.equal(holder1partitions[0], partitions.one);
      assert.equal(holder1partitions[1], partitions.two);
      assert.equal(holder2partitions[0], partitions.one);
      assert.equal(holder2partitions[1], partitions.two);
    });

    it("should not emit ERC-20 transfer event if no owner change", async () => {
      // Certificate for transfer from partition 1 to partition 2 (TH1)
      const certificate = transferSignedCertificate(
        contractAddress,
        tokenHolder1,
        tokenHolder1,
        tokenHolder1,
        partitions.one,
        partitions.two,
        1
      );

      const totalSupply = await erc1400.totalSupply();

      // Transfer, check we emit TransferByPartition event, but no Transfer (ERC-20) event
      await checkTxEvents(
        contractsWithEvents,
        erc1400.operatorTransferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder1,
          1,
          certificate,
          changePartitionData(partitions.two),
          { from: tokenHolder1 }
        ),
        EventObjects.transferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder1,
          tokenHolder1,
          1,
          certificate,
          changePartitionData(partitions.two)
        )
        //EventObjects.transferBetweenPartition(
        //  partitions.one,
        //  partitions.two,
        //  10
        //)
      );

      // Check balances
      await Asserts.balanceOf(erc1400, tokenHolder1, 70);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder1,
        34
      );
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.two,
        tokenHolder1,
        36
      );
      // Check total supply did not change
      await Asserts.totalSupply(erc1400, totalSupply);
    });
  });

  describe("canTransferByPartition", async () => {
    it("should authorize transfer from holder 1 to holder2", async () => {
      const transferCertificate = transferSignedCertificate(
        contractAddress,
        tokenHolder1,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.one,
        5
      );

      const result = await erc1400.canTransferByPartition.call(
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        5,
        transferCertificate,
        { from: tokenHolder1 }
      );

      assert.isTrue(result.status === "0x01" || result.status === "0x03");
    });

    it("should authorize transfer tokens from holder 2 to holder 1 ", async () => {
      const transferCertificate = transferSignedCertificate(
        contractAddress,
        tokenHolder2,
        tokenHolder2,
        tokenHolder1,
        partitions.one,
        partitions.one,
        10
      );

      const result = await erc1400.canTransferByPartition.call(
        tokenHolder2,
        tokenHolder1,
        partitions.one,
        10,
        transferCertificate,
        { from: tokenHolder2 }
      );

      assert.isTrue(result.status === "0x01" || result.status === "0x03");
    });

    it("should return insufficient balance for empty partition", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        tokenHolder1,
        tokenHolder1,
        tokenHolder2,
        partitions.three,
        partitions.three,
        10
      );

      const result = await erc1400.canTransferByPartition.call(
        tokenHolder1,
        tokenHolder2,
        partitions.three,
        10,
        certificate,
        { from: tokenOwner }
      );

      assert.equal(result.status, "0x54");
    });

    it("should return insufficient balance error", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        tokenHolder1,
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        partitions.one,
        40
      );

      const result = await erc1400.canTransferByPartition.call(
        tokenHolder1,
        tokenHolder2,
        partitions.one,
        40,
        certificate,
        { from: tokenOwner }
      );

      assert.equal(result.status, "0x54");
    });

    it("should return invalid address error", async () => {
      const result = await erc1400.canTransferByPartition.call(
        tokenHolder1,
        ZERO_ADDRESS,
        partitions.one,
        10,
        transferSignedCertificate(
          contractAddress,
          tokenHolder1,
          tokenHolder1,
          ZERO_ADDRESS,
          partitions.one,
          partitions.one,
          10
        )
      );
      assert.equal(result.status, 0x20);
    });
  });

  describe("Redeem", async () => {
    it("should fail when the value is 0 ", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        tokenHolder2,
        tokenHolder2,
        partitions.one,
        0
      );

      await Reverts.assertPositiveNumberOfTokens(
        erc1400.redeemByPartition(partitions.one, 0, certificate, {
          from: tokenHolder2,
        })
      );
    });

    it("should fail when insufficient balance on partition", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        tokenHolder2,
        tokenHolder2,
        partitions.one,
        1000
      );

      await Reverts.assertInsufficientBalance(
        erc1400.redeemByPartition(partitions.one, 1000, certificate, {
          from: tokenHolder2,
        })
      );
    });

    it("should fail when balance is insufficient", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        tokenHolder2,
        tokenHolder2,
        partitions.two,
        70
      );

      await Reverts.assertInsufficientBalance(
        erc1400.redeemByPartition(partitions.two, 70, certificate, {
          from: tokenHolder2,
        })
      );
    });

    it("should succeed", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        tokenHolder2,
        tokenHolder2,
        partitions.one,
        10
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.redeemByPartition(partitions.one, 10, certificate, {
          from: tokenHolder2,
        }),
        EventObjects.redeemedByPartition(
          partitions.one,
          tokenHolder2,
          tokenHolder2,
          10,
          certificate
        ),
        EventObjects.redeemed(tokenHolder2, tokenHolder2, 10, certificate),
        EventObjects.transfer(tokenHolder2, ZERO_ADDRESS, 10)
      );

      await Asserts.balanceOf(erc1400, tokenHolder2, 50);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.one,
        tokenHolder2,
        35
      );
      await Asserts.totalSupply(erc1400, 120);
    });
  });
});
