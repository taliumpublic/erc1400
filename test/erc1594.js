import Asserts from "./helpers/asserts";
import {
  action_codes,
  createSignedCertificate,
  transferSignedCertificate,
  issueSignedCertificate,
  redeemSignedCertificate,
  generateCommonParams,
  generateFunctionParams,
  partitions,
  signer,
  ZERO_ADDRESS,
} from "./helpers/common";
import OpCodeAssertion from "./helpers/errorcodes";
import { checkTxEvents, EventObjects } from "./helpers/event_asserts";
import Reverts from "./helpers/reverts";

const ERC1400 = artifacts.require("./StandaloneERC1400.sol");
const TransactionValidator = artifacts.require(
  "./validation/TransactionValidator.sol"
);

contract("ERC1594", (accounts) => {
  const tokenHolder1 = accounts[3];
  const tokenHolder2 = accounts[2];
  const operator = accounts[5];
  const tokenOwner = accounts[4];
  const storageOwner = accounts[8];
  const controller = accounts[7];

  // address of a DVP, ... anything that could execute token transfer (or else) for a tokenHolder
  // without him to be an operator or a controller
  // will test of the allowance system
  const allowanceAddress = accounts[6];

  const empty_data = "0x0000000000000000000000000000000000000000";

  let erc1400;
  let transactionValidator;
  let contractAddress;
  let contractsWithEvents;
  before(async () => {
    erc1400 = await ERC1400.new("", "", 0, partitions.default, controller, {
      from: tokenOwner,
    });

    transactionValidator = await TransactionValidator.new(
      erc1400.address,
      signer,
      {
        from: tokenOwner,
      }
    );

    erc1400.configure(transactionValidator.address, {
      from: tokenOwner,
    });
    contractAddress = erc1400.address;
    contractsWithEvents = [erc1400];
  });

  describe(`Issuance`, async () => {
    it("should have issuance to true when deployed", async () => {
      assert.isTrue(await erc1400.isIssuable());
    });

    it("should fail in issue the tokens because the certificate is empty", async () => {
      await Reverts.assertEmptyCertificate(
        erc1400.issue(tokenHolder1, 100, "0x0", {
          from: tokenHolder2,
        })
      );
    });

    it("should fail in issue the tokens to the token holders because msg.sender does not match certificate", async () => {
      await Reverts.assertInvalidCertificate(
        erc1400.issue(
          tokenHolder1,
          100,
          issueSignedCertificate(
            contractAddress,
            tokenOwner,
            tokenHolder1,
            partitions.default,
            100
          ),
          {
            from: tokenHolder2,
          }
        )
      );
    });

    it("should fail in issuance the tokens because to address is zero address", async () => {
      await Reverts.assertInvalidAddress(
        erc1400.issue(
          ZERO_ADDRESS,
          100,
          issueSignedCertificate(
            contractAddress,
            tokenOwner,
            ZERO_ADDRESS,
            partitions.default,
            100
          ),
          {
            from: tokenOwner,
          }
        )
      );
    });

    it("should successfully issue the tokens to the token holder", async () => {
      const amount = 1000;

      const mintCertificate = issueSignedCertificate(
        contractAddress,
        tokenOwner, // Sender
        tokenHolder1,
        partitions.default,
        amount
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.issue(tokenHolder1, amount, mintCertificate, {
          from: tokenOwner,
        }),
        EventObjects.issuedByPartition(
          partitions.default,
          tokenHolder1,
          amount,
          mintCertificate
        ),
        EventObjects.issued(tokenOwner, tokenHolder1, amount, mintCertificate),
        EventObjects.transfer(ZERO_ADDRESS, tokenHolder1, amount)
      );

      // verify the balance
      await Asserts.balanceOf(erc1400, tokenHolder1, amount);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        amount
      );
      await Asserts.totalSupply(erc1400, amount);
    });

    it("should successfully issue tokens to more token holders", async () => {
      const amount = 500;

      const mintCertificate = issueSignedCertificate(
        contractAddress,
        tokenOwner, // Sender
        tokenHolder2,
        partitions.default,
        amount
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.issue(tokenHolder2, amount, mintCertificate, {
          from: tokenOwner,
        }),
        EventObjects.issuedByPartition(
          partitions.default,
          tokenHolder2,
          amount,
          mintCertificate
        ),
        EventObjects.issued(tokenOwner, tokenHolder2, amount, mintCertificate),
        EventObjects.transfer(ZERO_ADDRESS, tokenHolder2, amount)
      );

      // verify the balance
      await Asserts.balanceOf(erc1400, tokenHolder2, amount);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder2,
        amount
      );
      await Asserts.totalSupply(erc1400, 1500);
    });

    it("should detect overflow", async () => {
      // Max uint256
      const amountToIssue = web3.utils.toBN(
        "0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
      );
      const mintCertificate = issueSignedCertificate(
        contractAddress,
        tokenOwner, // Sender
        tokenHolder2,
        partitions.default,
        amountToIssue
      );

      await Reverts.assertOverflow(
        erc1400.issue(tokenHolder2, amountToIssue, mintCertificate, {
          from: tokenOwner,
        })
      );
    });
  });

  describe("canTransfer", async () => {
    it("should successfully call canTransfer", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        tokenHolder1, // Sender
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        partitions.default,
        100
      );

      const result = await erc1400.canTransfer(tokenHolder2, 100, certificate, {
        from: tokenHolder1,
      });

      assert.isTrue(result.status === "0x01" || result.status === "0x03");
    });

    it("should successfully call canTransferFrom", async () => {
      await erc1400.approve(allowanceAddress, 150, {
        from: tokenHolder1,
      });

      assert.equal(
        await erc1400.allowance(tokenHolder1, allowanceAddress),
        150
      );

      const certificate = transferSignedCertificate(
        contractAddress,
        allowanceAddress, // Sender
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        partitions.default,
        100
      );

      const result = await erc1400.canTransferFrom(
        tokenHolder1,
        tokenHolder2,
        100,
        certificate,
        { from: allowanceAddress }
      );

      assert.equal(result.status, "0x01");

      await erc1400.approve(allowanceAddress, 0, { from: tokenHolder1 });
    });

    it("canTransferFrom should fail if allowance insufficient", async () => {
      await erc1400.approve(allowanceAddress, 50, { from: tokenHolder1 });

      const certificate = transferSignedCertificate(
        contractAddress,
        allowanceAddress, // Sender
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        partitions.default,
        100
      );

      const result = await erc1400.canTransferFrom(
        tokenHolder1,
        tokenHolder2,
        100,
        certificate,
        { from: allowanceAddress }
      );

      assert.equal(result.status, "0x56");

      await erc1400.approve(allowanceAddress, 0, { from: tokenHolder1 });
    });
  });

  describe("Transfer (transferWithData)", async () => {
    it("should fail to transfer the tokens when the sender doesn't have the sufficient balance", async () => {
      await Reverts.assertInsufficientBalance(
        erc1400.transferWithData(
          tokenHolder2,
          1100,
          transferSignedCertificate(
            contractAddress,
            tokenHolder1,
            tokenHolder1,
            tokenHolder2,
            partitions.default,
            partitions.default,
            1100
          ),
          {
            from: tokenHolder1,
          }
        )
      );
    });

    it("should fail to transfer the tokens to the zero address", async () => {
      await Reverts.assertInvalidAddress(
        erc1400.transferWithData(
          ZERO_ADDRESS,
          110,
          transferSignedCertificate(
            contractAddress,
            tokenHolder1,
            tokenHolder1,
            ZERO_ADDRESS,
            partitions.default,
            partitions.default,
            110
          ),
          {
            from: tokenHolder1,
          }
        )
      );
    });

    it("should successfully transfer the tokens from one holder to another", async () => {
      const amount = 100;

      const certificate = transferSignedCertificate(
        contractAddress,
        tokenHolder1, // Sender
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        partitions.default,
        amount
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.transferWithData(tokenHolder2, amount, certificate, {
          from: tokenHolder1,
        }),
        EventObjects.transferByPartition(
          partitions.default,
          tokenHolder1,
          tokenHolder1,
          tokenHolder2,
          amount,
          certificate,
          null
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, amount)
      );

      await Asserts.balanceOf(erc1400, tokenHolder1, 900);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        900
      );
      await Asserts.balanceOf(erc1400, tokenHolder2, 600);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder2,
        600
      );
    });
  });

  describe(`Approval system (transferFromWithData)`, async () => {
    it("should fail to transfer the tokens (transferFromWithData) without allowance set", async () => {
      const amount = 200;

      const certificate = transferSignedCertificate(
        contractAddress,
        allowanceAddress, // Sender
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        partitions.default,
        amount
      );

      await Reverts.assertInsufficientAllowance(
        erc1400.transferFromWithData(
          tokenHolder1,
          tokenHolder2,
          amount,
          certificate,
          { from: allowanceAddress }
        )
      );
    });

    it("should not prevent approval is balance is insufficient", async () => {
      erc1400.approve(allowanceAddress, 1000, {
        from: tokenHolder1,
      });
    });

    it("should succeed to approve if the tokenHolder has sufficient balance", async () => {
      const expectedTotalSupply = 1500;
      const expectedTokenHolder1Balance = 900;
      const expectedTokenHolder1BalanceOnDefaultPartition = 900;
      const amountToApprove = 100;

      // We approve the use of some of these new tokens to the allowanceAddress
      await checkTxEvents(
        contractsWithEvents,
        erc1400.approve(allowanceAddress, amountToApprove, {
          from: tokenHolder1,
        }),
        EventObjects.approval(tokenHolder1, allowanceAddress, amountToApprove)
      );
      // check totalSupply did not change
      await Asserts.totalSupply(erc1400, expectedTotalSupply);
      // check tokenHolder1 balances did not change
      await Asserts.balanceOf(
        erc1400,
        tokenHolder1,
        expectedTokenHolder1Balance
      );
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        expectedTokenHolder1BalanceOnDefaultPartition
      );
      // check tokenHolder1 approval concerning the approved address is correct
      await Asserts.allowanceOf(
        erc1400,
        tokenHolder1,
        allowanceAddress,
        amountToApprove
      );
    });

    it("should not intertwine multiple allowances", async () => {
      // We approve the use of some of these new tokens to the allowanceAddress
      await checkTxEvents(
        contractsWithEvents,
        erc1400.approve(allowanceAddress, 500, {
          from: tokenHolder2,
        }),
        EventObjects.approval(tokenHolder2, allowanceAddress, 500)
      );
      // check tokenHolder1 approval concerning the approved address is correct
      await Asserts.allowanceOf(erc1400, tokenHolder1, allowanceAddress, 100);
      // check tokenHolder2 approval concerning the approved address is correct
      await Asserts.allowanceOf(erc1400, tokenHolder2, allowanceAddress, 500);
      // check tokenHolder1 approval concerning another address is 0
      await Asserts.allowanceOf(erc1400, tokenHolder1, tokenHolder2, 0);
      await Asserts.allowanceOf(erc1400, tokenHolder2, tokenHolder1, 0);
    });

    it("should allow updating allowances with increaseAllowance", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.increaseAllowance(allowanceAddress, 150, {
          from: tokenHolder1,
        }),
        // approve shall display the total amount allowed
        EventObjects.approval(tokenHolder1, allowanceAddress, 250)
      );
      // check tokenHolder1 approval concerning the approved address is correct
      await Asserts.allowanceOf(erc1400, tokenHolder1, allowanceAddress, 250);
    });

    it("should not fail to update allowance is increaseAllowance causes allowance to be superior to tokenHolder's balance", async () => {
      erc1400.increaseAllowance(allowanceAddress, 2000, {
        from: tokenHolder1,
      });
      await Asserts.allowanceOf(erc1400, tokenHolder1, allowanceAddress, 2250);
    });

    it("should allow updating allowances with decreaseAllowance", async () => {
      await checkTxEvents(
        contractsWithEvents,
        erc1400.decreaseAllowance(allowanceAddress, 2200, {
          from: tokenHolder1,
        }),
        // approve shall display the total amount allowed
        EventObjects.approval(tokenHolder1, allowanceAddress, 50)
      );
      // check tokenHolder1 approval concerning the approved address is correct
      await Asserts.allowanceOf(erc1400, tokenHolder1, allowanceAddress, 50);
    });

    it("should fail updating allowances if decreaseAllowance causes allowance to be negative", async () => {
      await Reverts.assertSubstractOverflow(
        erc1400.decreaseAllowance(allowanceAddress, 1000, {
          from: tokenHolder1,
        })
      );
    });

    it("should fail to transfer token if tokenHolder as enough balance but spender has not enough allowance", async () => {
      await Reverts.assertInsufficientAllowance(
        erc1400.transferFromWithData(
          tokenHolder1,
          tokenHolder2,
          200,
          transferSignedCertificate(
            contractAddress,
            allowanceAddress,
            tokenHolder1,
            tokenHolder2,
            partitions.default,
            partitions.default,
            200
          ),
          { from: allowanceAddress }
        )
      );
    });

    it("should succeed to transfer tokens if the tokenHolder has sufficient balance and correct allowance", async () => {
      const amountTransfered = 50;
      const expectedTotalSupply = 1500;
      const expectedTokenHolder1BalanceAfterTransfer = 850;
      const expectedTokenHolder2BalanceAfterTransfer = 650;
      const expectedTokenHolder1BalanceOnDefaultPartitionAfterTransfer = 850;
      const expectedTokenHolder2BalanceOnDefaultPartitionAfterTransfer = 650;
      const expectedAllowanceAfterTransfer = 0;

      // A certificate is necessary for that transfer to be accepted
      const certificate = transferSignedCertificate(
        contractAddress,
        allowanceAddress,
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        partitions.default,
        amountTransfered
      );

      // We execute token transfer from tokenHolder1 to tokenHolder2 by the allowanceAddress
      await checkTxEvents(
        contractsWithEvents,
        erc1400.transferFromWithData(
          tokenHolder1,
          tokenHolder2,
          amountTransfered,
          certificate,
          { from: allowanceAddress }
        ),
        EventObjects.transferByPartition(
          partitions.default,
          allowanceAddress,
          tokenHolder1,
          tokenHolder2,
          amountTransfered,
          certificate,
          null
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, amountTransfered),
        EventObjects.approval(
          tokenHolder1,
          allowanceAddress,
          expectedAllowanceAfterTransfer
        )
      );
      // check totalSupply did not change
      await Asserts.totalSupply(erc1400, expectedTotalSupply);
      // check tokenHolder1 balances
      await Asserts.balanceOf(
        erc1400,
        tokenHolder1,
        expectedTokenHolder1BalanceAfterTransfer
      );
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        expectedTokenHolder1BalanceOnDefaultPartitionAfterTransfer
      );
      // check tokenHolder2 balances
      await Asserts.balanceOf(
        erc1400,
        tokenHolder2,
        expectedTokenHolder2BalanceAfterTransfer
      );
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder2,
        expectedTokenHolder2BalanceOnDefaultPartitionAfterTransfer
      );
      // check allowance decrease too
      await Asserts.allowanceOf(erc1400, tokenHolder1, allowanceAddress, 0);
    });

    it("should fail to transfer tokens if the tokenHolder approved more than its balance", async () => {
      // Add an operator to the account TH2
      await erc1400.authorizeOperator(operator, { from: tokenHolder2 });

      // Male the operator execute a transfer so as balance < allowance(allowanceAddress)
      const certificate = transferSignedCertificate(
        contractAddress,
        operator,
        tokenHolder2,
        tokenHolder1,
        partitions.default,
        partitions.default,
        600
      );

      await erc1400.operatorTransferByPartition(
        partitions.default,
        tokenHolder2,
        tokenHolder1,
        600,
        certificate,
        "0x0",
        { from: operator }
      );

      // try to execute the transfer from the allowanceAddress
      // balance < amount <= allowanceAmount
      // and verify failure
      await Reverts.assertInsufficientBalance(
        erc1400.transferFromWithData(
          tokenHolder2,
          tokenHolder1,
          400,
          transferSignedCertificate(
            contractAddress,
            allowanceAddress,
            tokenHolder2,
            tokenHolder1,
            partitions.default,
            partitions.default,
            400
          ),
          { from: allowanceAddress }
        )
      );

      // check allowance remains unchanged
      await Asserts.allowanceOf(erc1400, tokenHolder2, allowanceAddress, 500);
    });
  });

  describe("Redeem", async () => {
    it("should failed to redeem the tokens because balance is less than the value", async () => {
      await Reverts.assertInsufficientBalance(
        erc1400.redeem(
          1200,
          redeemSignedCertificate(
            contractAddress,
            tokenHolder2,
            tokenHolder2,
            partitions.default,
            1200
          ),
          {
            from: tokenHolder2,
          }
        )
      );
    });

    it("should successfully redeem the tokens by the msg.sender", async () => {
      const amount = 200;

      const certificate = redeemSignedCertificate(
        contractAddress,
        tokenHolder1,
        tokenHolder1,
        partitions.default,
        amount
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.redeem(amount, certificate, {
          from: tokenHolder1,
        }),
        EventObjects.redeemedByPartition(
          partitions.default,
          tokenHolder1,
          tokenHolder1,
          amount,
          certificate
        ),
        EventObjects.redeemed(tokenHolder1, tokenHolder1, amount, certificate)
      );

      await Asserts.totalSupply(erc1400, 1300);
      await Asserts.balanceOf(erc1400, tokenHolder1, 1250);
    });

    it("should fail to redeem the tokens because `to` address is the zero type address", async () => {
      await Reverts.assertInvalidAddress(
        erc1400.redeemFrom(
          ZERO_ADDRESS,
          100,
          redeemSignedCertificate(
            contractAddress,
            operator,
            ZERO_ADDRESS,
            partitions.default,
            100
          ),
          {
            from: operator,
          }
        )
      );
    });

    it("should successfully redeem the tokens (redeemFrom)", async () => {
      const amount = 50;

      const certificate = redeemSignedCertificate(
        contractAddress,
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        amount
      );

      await checkTxEvents(
        contractsWithEvents,
        erc1400.redeemFrom(tokenHolder2, amount, certificate, {
          from: tokenHolder1,
        }),
        EventObjects.redeemedByPartition(
          partitions.default,
          tokenHolder1,
          tokenHolder2,
          amount,
          certificate
        ),
        EventObjects.redeemed(tokenHolder1, tokenHolder2, amount, certificate),
        EventObjects.transfer(tokenHolder2, ZERO_ADDRESS, amount)
      );

      await Asserts.totalSupply(erc1400, 1250);
      await Asserts.balanceOf(erc1400, tokenHolder2, 0);
    });
  });
});
