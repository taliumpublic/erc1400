import Asserts from "./helpers/asserts";
import {
  action_codes,
  changePartitionData,
  createSignedCertificate,
  transferSignedCertificate,
  issueSignedCertificate,
  redeemSignedCertificate,
  generateCommonParams,
  generateFunctionParams,
  partitions,
  signer,
  ZERO_ADDRESS,
} from "./helpers/common";
import { checkTxEvents, EventObjects } from "./helpers/event_asserts";
import Reverts from "./helpers/reverts";

const ERC1400 = artifacts.require("./StandaloneERC1400.sol");
const TransactionValidator = artifacts.require(
  "./validation/TransactionValidator.sol"
);

contract("ERC1400", (accounts) => {
  const tokenHolder1 = accounts[3];
  const tokenHolder2 = accounts[2];
  const operator1 = accounts[5];
  const operator2 = accounts[6];
  const controller = accounts[8];
  const storageOwner = accounts[7];
  const tokenOwner = accounts[9];

  const additionalController = "0x0a1cf2df4af24e0a67c5868ef28c0c4a8e5adeb5";

  let erc1400;
  let transactionValidator;
  let contractAddress;
  let contractsWithEvents;

  before(async () => {
    erc1400 = await ERC1400.new("", "", 0, partitions.default, controller, {
      from: tokenOwner,
    });

    transactionValidator = await TransactionValidator.new(
      erc1400.address,
      signer,
      {
        from: tokenOwner,
      }
    );

    erc1400.configure(transactionValidator.address, {
      from: tokenOwner,
    });
    contractAddress = erc1400.address;
    contractsWithEvents = [erc1400];

    // Issue some tokens for the tests
    const certificate = issueSignedCertificate(
      contractAddress,
      tokenOwner,
      tokenHolder1,
      partitions.default,
      500
    );
    await erc1400.issueByPartition(
      partitions.default,
      tokenHolder1,
      500,
      certificate,
      {
        from: tokenOwner,
      }
    );
    await Asserts.balanceOf(erc1400, tokenHolder1, 500);
    await Asserts.balanceOfByPartition(
      erc1400,
      partitions.default,
      tokenHolder1,
      500
    );
  });

  describe("Controllable on deployment", async () => {
    it("should be controllable when contract is deployed", async () => {
      assert.isTrue(await erc1400.isControllable());
    });

    it("should recognize controller passed in constructor", async () => {
      assert.isTrue(await erc1400.isController(controller));
    });

    it("should not recognize other address as controller", async () => {
      assert.isFalse(await erc1400.isController(tokenOwner));
      assert.isFalse(await erc1400.isController(tokenHolder1));
    });

    it("should add new controller", async () => {
      await erc1400.addController(additionalController, {
        from: tokenOwner,
      });

      // Newly added controller
      assert.isTrue(await erc1400.isController.call(additionalController));
    });

    it("should revoke controller", async () => {
      assert.isTrue(await erc1400.isController.call(additionalController));

      await erc1400.revokeController(additionalController, {
        from: tokenOwner,
      });

      // Newly added controller
      assert.isFalse(await erc1400.isController.call(additionalController));
    });
  });

  describe("Controller Actions", async () => {
    it("should operatorTransferByPartition succeed from controller", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        controller,
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        partitions.default,
        10
      );

      // Transfer 10 tokens from TH1 to TH2
      await checkTxEvents(
        contractsWithEvents,
        erc1400.operatorTransferByPartition(
          partitions.default,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          "0x0",
          { from: controller }
        ),
        EventObjects.transferByPartition(
          partitions.default,
          controller,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          "0x00"
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, 10),
        EventObjects.controllerTransfer(
          controller,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          "0x00"
        )
      );

      // Check balances: TH1 should now have 490 (= 500 - 10), TH2 should have 10
      await Asserts.balanceOf(erc1400, tokenHolder1, 490);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        490
      );
      await Asserts.balanceOf(erc1400, tokenHolder2, 10);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder2,
        10
      );
      await Asserts.totalSupply(erc1400, 500);
    });

    it("should operatorRedeemByPartition succeed from controller", async () => {
      // create certificate
      const certificate = redeemSignedCertificate(
        contractAddress,
        controller,
        tokenHolder1,
        partitions.default,
        10
      );

      // execute transaction
      await checkTxEvents(
        contractsWithEvents,
        erc1400.operatorRedeemByPartition(
          partitions.default,
          tokenHolder1,
          10,
          certificate,
          "0x00",
          { from: controller }
        ),
        EventObjects.redeemedByPartition(
          partitions.default,
          controller,
          tokenHolder1,
          10,
          certificate,
          "0x00"
        ),
        EventObjects.redeemed(
          controller,
          tokenHolder1,
          10,
          certificate,
          "0x00"
        ),
        EventObjects.transfer(tokenHolder1, ZERO_ADDRESS, 10),
        EventObjects.controllerRedemption(
          controller,
          tokenHolder1,
          10,
          certificate,
          0x00
        )
      );

      // Check balances
      await Asserts.balanceOf(erc1400, tokenHolder1, 480);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        480
      );
      await Asserts.totalSupply(erc1400, 490);
    });

    it("should allow transfer between partitions", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        controller,
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        partitions.two,
        10
      );

      // execute transaction
      await checkTxEvents(
        contractsWithEvents,
        erc1400.operatorTransferByPartition(
          partitions.default,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          changePartitionData(partitions.two),
          { from: controller }
        ),
        EventObjects.transferByPartition(
          partitions.default,
          controller,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          changePartitionData(partitions.two)
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, 10),
        EventObjects.controllerTransfer(
          controller,
          tokenHolder1,
          tokenHolder2,
          10,
          certificate,
          changePartitionData(partitions.two)
        )
      );

      // check balances
      await Asserts.balanceOf(erc1400, tokenHolder1, 470);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.default,
        tokenHolder1,
        470
      );
      await Asserts.balanceOf(erc1400, tokenHolder2, 20);
      await Asserts.balanceOfByPartition(
        erc1400,
        partitions.two,
        tokenHolder2,
        10
      );
      await Asserts.totalSupply(erc1400, 490);
    });
  });

  describe("Controller transfer", async () => {
    it("should revert when msg.sender is not controller", async () => {
      await Reverts.assertNotController(
        erc1400.operatorTransferByPartition(
          partitions.default,
          tokenHolder1,
          tokenHolder2,
          200,
          transferSignedCertificate(
            contractAddress,
            tokenHolder1,
            tokenHolder1,
            tokenHolder2,
            partitions.default,
            partitions.default,
            200
          ),
          "0x0",
          { from: tokenHolder2 }
        )
      );
    });

    it("should revert in insufficient balance case", async () => {
      await Reverts.assertInsufficientBalance(
        erc1400.operatorTransferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder2,
          600,
          transferSignedCertificate(
            contractAddress,
            controller,
            tokenHolder1,
            tokenHolder2,
            partitions.one,
            partitions.one,
            600
          ),
          "0x0",
          { from: controller }
        )
      );
    });

    it("should revert in zero_address as destination case", async () => {
      const certificate = transferSignedCertificate(
        contractAddress,
        controller,
        tokenHolder1,
        ZERO_ADDRESS,
        partitions.default,
        partitions.default,
        10
      );

      await Reverts.assertInvalidAddress(
        erc1400.operatorTransferByPartition(
          partitions.default,
          tokenHolder1,
          ZERO_ADDRESS,
          10,
          certificate,
          "0x0",
          { from: controller }
        )
      );
    });

    it("should succeed", async () => {
      const amount = 200;

      const certificate = transferSignedCertificate(
        contractAddress,
        controller,
        tokenHolder1,
        tokenHolder2,
        partitions.default,
        partitions.default,
        amount
      );

      await checkTxEvents(
        [erc1400],
        erc1400.operatorTransferByPartition(
          partitions.default,
          tokenHolder1,
          tokenHolder2,
          amount,
          certificate,
          "0x0",
          { from: controller }
        ),
        EventObjects.transferByPartition(
          partitions.default,
          controller,
          tokenHolder1,
          tokenHolder2,
          amount,
          certificate,
          "0x00"
        ),
        EventObjects.transfer(tokenHolder1, tokenHolder2, amount),
        EventObjects.controllerTransfer(
          controller,
          tokenHolder1,
          tokenHolder2,
          amount,
          certificate,
          "0x00"
        )
      );

      await Asserts.balanceOf(erc1400, tokenHolder1, 270);
      await Asserts.balanceOf(erc1400, tokenHolder2, 220);
    });
  });

  describe("Controller redeem", async () => {
    it("should revert during operatorRedeemByPartition() because msg.sender is not authorised", async () => {
      await Reverts.assertNotController(
        erc1400.operatorRedeemByPartition(
          partitions.one,
          tokenHolder2,
          100,
          "0x0",
          "0x0",
          {
            from: tokenOwner,
          }
        )
      );
    });

    it("should revert during operatorRedeemByPartition() because tokenHolder doesn't have sufficent balance", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        controller,
        tokenHolder2,
        partitions.one,
        400
      );

      await Reverts.assertInsufficientBalance(
        erc1400.operatorRedeemByPartition(
          partitions.one,
          tokenHolder2,
          400,
          certificate,
          "0x0",
          {
            from: controller,
          }
        )
      );
    });

    it("should revert during operatorRedeemByPartition() if 0x address", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        controller,
        ZERO_ADDRESS,
        partitions.default,
        1
      );

      await Reverts.assertInvalidAddress(
        erc1400.operatorRedeemByPartition(
          partitions.default,
          ZERO_ADDRESS,
          1,
          certificate,
          "0x0",
          {
            from: controller,
          }
        )
      );
    });

    it("should succeed", async () => {
      const certificate = redeemSignedCertificate(
        contractAddress,
        controller,
        tokenHolder2,
        partitions.default,
        1
      );

      await checkTxEvents(
        [erc1400],
        erc1400.operatorRedeemByPartition(
          partitions.default,
          tokenHolder2,
          1,
          certificate,
          "0x0",
          {
            from: controller,
          }
        ),
        EventObjects.redeemedByPartition(
          partitions.default,
          controller,
          tokenHolder2,
          1,
          certificate,
          "0x00"
        ),
        EventObjects.redeemed(controller, tokenHolder2, 1, certificate),
        EventObjects.transfer(tokenHolder2, ZERO_ADDRESS, 1),
        EventObjects.controllerRedemption(
          controller,
          tokenHolder2,
          1,
          certificate,
          "0x00"
        )
      );

      await Asserts.balanceOf(erc1400, tokenHolder2, 219);
    });
  });

  describe("Controller role", async () => {
    it("should revert when msg.sender is not the owner", async () => {
      await Reverts.assertNotOwner(
        erc1400.addController(tokenHolder1, { from: tokenHolder1 })
      );
    });
    it("should revert if new controller is zero address", async () => {
      await Reverts.assertControllerCanNotBeZeroAddress(
        erc1400.addController(ZERO_ADDRESS, { from: tokenOwner })
      );
    });
    it("should succeed", async () => {
      await erc1400.addController(tokenHolder1, {
        from: tokenOwner,
      });
      assert.isTrue(await erc1400.isController(tokenHolder1));
    });
  });

  describe("Finalization", async () => {
    it("Should revert on when msg.sender is not the owner", async () => {
      await Reverts.assertNotOwner(
        erc1400.terminateControl({ from: controller })
      );
    });

    it("should be possible to terminate control", async () => {
      await erc1400.terminateControl({ from: tokenOwner });
    });

    it("should have isControllable at false when finalized", async () => {
      assert.isFalse(await erc1400.isControllable());
    });

    it("should prevent controller transfer", async () => {
      await Reverts.assertControllerFinalized(
        erc1400.operatorTransferByPartition(
          partitions.one,
          tokenHolder1,
          tokenHolder2,
          200,
          "0x0",
          "0x0",
          { from: controller }
        )
      );
    });
    it("should prevent controller redeem", async () => {
      await Reverts.assertControllerFinalized(
        erc1400.operatorRedeemByPartition(
          partitions.one,
          tokenHolder2,
          50,
          "0x0",
          "0x0",
          {
            from: controller,
          }
        )
      );
    });
  });
});
