require("babel-register");
require("babel-polyfill");

module.exports = {
  plugins: ["truffle-contract-size", "solidity-coverage"],

  networks: {
    dev_local: {
      host: "0.0.0.0",
      port: "8550",
      network_id: "*",
      gas: 0x6691b7,
      gasPrice: 0x01,
      timeoutBlocks: 3,
    },
    parity_network: {
      host: "0.0.0.0",
      network_id: "*",
      port: 8550,
      gas: 0x5b8d8043,
      gasPrice: 0x01,
      from: "0x...", // address of the unlocked account of the node
    },
  },
  compilers: {
    solc: {
      version: "^0.7.0",
      optimizer: {
        enabled: true,
        runs: 1,
      },
    },
  },
  mocha: {
    enableTimeouts: false,
  },
};
